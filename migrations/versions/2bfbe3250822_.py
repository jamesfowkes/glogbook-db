"""empty message

Revision ID: 2bfbe3250822
Revises: d40b2c7300e5
Create Date: 2024-11-24 13:31:59.117878

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2bfbe3250822'
down_revision = 'd40b2c7300e5'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('glider_identifiers', schema=None) as batch_op:
        batch_op.create_index('unique_reg_when_type_is_reg', ['identifier'], unique=True, postgresql_where=sa.text('glider_identifier_type_id = 1'))
        batch_op.create_index('unique_trigraph_when_type_is_trigraph', ['identifier'], unique=True, postgresql_where=sa.text('glider_identifier_type_id = 2'))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('glider_identifiers', schema=None) as batch_op:
        batch_op.drop_index('unique_trigraph_when_type_is_trigraph', postgresql_where=sa.text('glider_identifier_type_id = 2'))
        batch_op.drop_index('unique_reg_when_type_is_reg', postgresql_where=sa.text('glider_identifier_type_id = 1'))

    # ### end Alembic commands ###
