"""empty message

Revision ID: 8a93b8dd9c87
Revises: 1581f4045799
Create Date: 2024-12-14 20:18:11.474067

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8a93b8dd9c87'
down_revision = '1581f4045799'
branch_labels = None
depends_on = None


def upgrade():

    op.rename_table('glider_types', 'glider_models')
    with op.batch_alter_table('glider_models', schema=None) as batch_op:
        batch_op.alter_column('glider_type_id', new_column_name='glider_model_id')

    with op.batch_alter_table('gliders', schema=None) as batch_op:
        batch_op.alter_column('glider_type_id', new_column_name='glider_model_id')
        batch_op.drop_constraint('gliders_glider_type_id_fkey', type_='foreignkey')
        batch_op.create_foreign_key('gliders_glider_model_id_fkey', 'glider_models', ['glider_model_id'], ['glider_model_id'])

def downgrade():

    op.rename_table('glider_models', 'glider_types')

    with op.batch_alter_table('glider_types', schema=None) as batch_op:
        batch_op.alter_column('glider_model_id', new_column_name='glider_type_id')

    with op.batch_alter_table('gliders', schema=None) as batch_op:
        batch_op.alter_column('glider_model_id', new_column_name='glider_type_id')
        batch_op.drop_constraint('gliders_glider_model_id_fkey', type_='foreignkey')
        batch_op.create_foreign_key('gliders_glider_type_id_fkey', 'glider_types', ['glider_type_id'], ['glider_type_id'])
