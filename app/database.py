from flask_sqlalchemy import SQLAlchemy

session_options={
	"expire_on_commit": False # Keeps DB objects accessible for read after session has closed (useful for testing)
}

db = SQLAlchemy(session_options=session_options)

def init_app(app):
    db.init_app(app)
    return db