def mins_to_hhmm(mins):
    hh = mins // 60
    mm = mins - (hh * 60)
    return f"{hh}h:{mm}m" if hh > 0 else f"{mm}m"
