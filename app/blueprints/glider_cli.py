import sys
import click
from app.models.glider_model import GliderModel
from app.models.glider import Glider
from app.models.glider_id import GliderIdentifier
from app.models.enumerations import IdentifierType

from app.database import db

def print_glider(n, glider):
    print(f"({n}) {glider.all_identifiers(True):25s}{glider.glider_model.name:15s} ({glider.glider_id:03d})")

def validate_trigraph(trigraph):
    return len(trigraph) == 3 and all('A' <= letter <= 'Z' for letter in trigraph)

def init_cli(app):
    @app.cli.command("create_glider")
    @click.argument("glider_model")
    @click.option("--trigraph")
    @click.option("--comp_no")
    @click.option("--reg")
    def cli_create_glider(glider_model, trigraph, comp_no, reg):
        
        if not any([trigraph, comp_no, reg]):
            print(f"At least one of trigraph, comp. no or registration must be provided")
            sys.exit(-1)

        existing_trigraph_check = Glider.get_by_identifier(trigraph)
        if existing_trigraph_check:
            print(f"Trigraph {trigraph} already exists")
            sys.exit(-1)

        existing_reg_check = Glider.get_by_identifier(reg)
        if existing_reg_check:
            print(f"Registration {reg} already exists")
            sys.exit(-1)

        glider_model_record = db.session.execute(db.select(GliderModel).where(GliderModel.name == glider_model)).scalar()

        if glider_model_record is None:
            print(f"Could not find glider type {glider_model}")
            sys.exit(-1)

        if trigraph and not validate_trigraph(trigraph):
            print(f"Trigraph {trigraph} invalid, expected three letters")
            sys.exit(-1)

        if trigraph and reg == "gc":
            # This is shorthand for prefixing the trigraph with "G-C"
            reg = "G-C" + trigraph.upper()
        elif trigraph and reg == "gd":
            # This is shorthand for prefixing the trigraph with "G-D"
            reg = "G-D" + trigraph.upper()

        identifier_type_ids = [
            db.session.execute(db.select(IdentifierType).where(IdentifierType.description==desc)).scalar().identifier_type_id
            for desc in ["BGA trigraph", "Registration", "Competition Number"]
        ]

        data = {
            "glider_model_id": glider_model_record.glider_model_id,
        }

        try:
            glider = Glider(**data)
            db.session.add(glider)
            db.session.flush()

            identifiers = {identifier_type_ids[0]: trigraph,  identifier_type_ids[1]: reg, identifier_type_ids[2]: comp_no}
            for identifier_type_id, identifier_text in identifiers.items():
                if identifier_text and len(identifier_text):
                    identifier = GliderIdentifier(glider_id=glider.glider_id, glider_identifier_type_id=identifier_type_id, identifier=identifier_text)
                    db.session.add(identifier)
                    db.session.flush()

            db.session.commit()
        except Exception as e:
            print(f"Could not add glider: {e}")
            db.session.rollback()

    @app.cli.command("add_glider_id")
    @click.argument("existing_id")
    @click.option("--trigraph")
    @click.option("--comp_no")
    @click.option("--reg")
    def cli_add_glider_id(existing_id, trigraph, comp_no, reg):
        glider_id_record = db.session.execute(db.select(GliderIdentifier).where(GliderIdentifier.identifier == existing_id)).scalar()
        if glider_id_record is None:
            print(f"No glider with identifier {existing_id} found.")
            return

        identifier_type_ids = [
            db.session.execute(db.select(IdentifierType).where(IdentifierType.description==desc)).scalar().identifier_type_id
            for desc in ["BGA trigraph", "Registration", "Competition Number"]
        ]

        glider = glider_id_record.glider
        try:
            identifiers = {identifier_type_ids[0]: trigraph,  identifier_type_ids[1]: reg, identifier_type_ids[2]: comp_no}
            for identifier_type_id, identifier_text in identifiers.items():
                if identifier_text and len(identifier_text):
                    identifier = GliderIdentifier(glider_id=glider.glider_id, glider_identifier_type_id=identifier_type_id, identifier=identifier_text)
                    db.session.add(identifier)
                    db.session.flush()
            db.session.commit()
        except Exception as e:
            print(f"Could not add glider ID: {e}")
            db.session.rollback()

    @app.cli.command("list_gliders")
    def cli_list_gliders():
        gliders = db.session.execute(db.select(Glider)).scalars()
        for n, glider in enumerate(gliders):
            print_glider(n+1, glider)

    @app.cli.command("find_glider")
    @click.argument("identifier")
    def cli_find_glider(identifier):
        glider = Glider.get_by_identifier(identifier)
        if glider:
            print_glider("1", glider)
        else:
            print(f"{identifier} not found")