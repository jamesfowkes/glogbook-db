import click
from app.models.location import Location

from app.database import db

def init_cli(app):
    @app.cli.command("create_location")
    @click.argument("name")
    @click.argument("description")
    @click.argument("lat")
    @click.argument("lon")
    def cli_create_location(**kwargs):
        loc = Location(**kwargs)
        loc.save()

    @app.cli.command("list_locations")
    def cli_list_locations():
        locations = db.session.execute(db.select(Location)).scalars()
        for n, location in enumerate(locations):
            print(f"{n}: {location}")