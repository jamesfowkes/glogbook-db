from flask_restx import Resource, fields

def query_resource(ns, endpoint, cls, **kwargs):
    @ns.route(endpoint)
    class QResource(Resource):
        def get(self):
            return cls.request_query(**kwargs)

def post_get_resource(ns, api, endpoint, cls, create_model):

    @ns.route(endpoint)
    class PGResource(Resource):

        def get(self):
            return cls.request_all()
    
        @ns.expect(create_model)
        def post(self):
            result, response = cls.request_create(api.payload)
            if result:
                return response, 201
            else:
                return response, 400

    return PGResource

def get_put_delete_resource(
    ns, api,
    endpoint, endpoint_params, cls, edit_model,
    get_error_msg=None, delete_error_msg=None
    ):
    
    if get_error_msg is None:
        get_error_msg = str(type(cls)) + " '{}'  does not exist"

    if delete_error_msg is None:
        delete_error_msg = "Error deleting " + str(type(cls)) + " '{}', check is it not used"

    @ns.route(endpoint)
    @ns.param(*endpoint_params)
    class GPDResource(Resource):

        def get(self, id):
            try:
                result = cls.request_by_id(id).data()
                return result
            except Exception:
                return get_error_msg.format(id), 404
    
        @ns.expect(edit_model)
        def put(self, id):
            result, error = cls.request_edit(id, api.payload)
            if result:
                return result.data()
            else:
                return {"error":error}, 400

        def delete(self, id):
            result = cls.request_delete(id)
            if result is None:
                return get_error_msg.format(id), 404
            elif result ==  False:
                return delete_error_msg.format(id), 400
            else:
                return result

    return GPDResource