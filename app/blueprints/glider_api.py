import json

from flask import request
from flask_restx import Resource, fields

from app.blueprints.api_resource import post_get_resource, get_put_delete_resource
from app.models.glider import Glider

def init_api(api):

    glider_edit_model = api.model("Glider", {
        'glider_model_id': fields.Integer(description="The ID for the type of glider"),
        'trigraph': fields.String(description="The trigraph for this glider"),
        'comp_no': fields.String(description="The competition number for this glider"),
        'greg': fields.String(description="The G-registration for this glider"),
        }
    )

    glider_create_model = api.model("GliderCreate", {
        'glider_model_id': fields.Integer(required=True, description="The ID for the type of glider"),
        'trigraph': fields.String(required=True, description="The trigraph for this glider"),
        'comp_no': fields.String(description="The competition number for this glider"),
        'greg': fields.String(description="The G-registration for this glider"),
        }
    )

    ns = api.namespace('glider',
        description='A glider is a specific instance of a glider type, with associated identifiers (trigraph, registration, competition number).'
    )

    GlidersResource = post_get_resource(ns, api, "/", Glider, glider_create_model)
    GliderResource = get_put_delete_resource(
        ns, api, '/<int:id>', ('id', 'The glider ID'), Glider, glider_edit_model)
