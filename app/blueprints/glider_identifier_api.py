import json

from flask import request
from flask_restx import Resource, fields

from app.blueprints.api_resource import post_get_resource, get_put_delete_resource
from app.models.glider_id import GliderIdentifier

def init_api(api):

    glider_id_edit_model = api.model("GliderIdentifier", {
        'identifier': fields.String(description="New identifier text"),
        }
    )

    glider_id_create_model = api.model("GliderIdentifierCreate", {
        'glider_id': fields.Integer(required=True, description="Associated glider ID"),
        'identifier_type': fields.Integer(required=True, description="Identifier type ID"),
        'identifier': fields.String(required=True, description="New identifier text"),
        }
    )

    ns = api.namespace('glider_id', description='Each glider can have several identifiers associated with it.')

    GliderIdentifiersResource = post_get_resource(ns, api, "/", GliderIdentifier, glider_id_create_model)
    GliderIdentifierResource = get_put_delete_resource(
        ns, api, '/<int:id>', ('id', 'The glider identifier ID'), GliderIdentifier, glider_id_edit_model)
