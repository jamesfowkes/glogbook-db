import json
import decimal

from flask import request
from flask_restx import Resource, fields

from app.models.enumerations import Filetype

from app.blueprints.api_resource import post_get_resource, get_put_delete_resource

def init_api(api):

    filetype_edit_model = api.model("Filetype", {
        'filetype_id': fields.Integer(readonly=True, description="Unique filetype ID"),
        'description': fields.String(description="Description of filetype")
        }
    )

    filetype_create_model = api.model("FiletypeCreate", {
        'description': fields.String(required=True, description="Description")
        }
    )

    ns = api.namespace('filetype',
        description='Filetype objects provide behaviourial and contextual information for files attached to flights.'
    )

    FiletypesResource = post_get_resource(ns, api, "/", Filetype, filetype_create_model)
    FiletypeResource = get_put_delete_resource(
        ns, api, '/<int:id>', ('id', 'The filetype ID'), Filetype, filetype_edit_model)
