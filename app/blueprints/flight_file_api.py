import json

from flask import request
from flask_restx import Resource, fields

from app.blueprints.api_resource import post_get_resource, get_put_delete_resource
from app.models.flight_file import FlightFile

def init_api(api):

    flight_file_edit_model = api.model("FlightFile", {
        'filetype_id': fields.Integer(description="Filetype ID"),
        'link': fields.String(description="Hyperlink to this file"),
        }
    )

    flight_file_create_model = api.model("FlightFileCreate", {
        'flight_id': fields.Integer(required=True, description="Associated flight ID"),
        'filetype_id': fields.Integer(required=True, description="Filetype ID"),
        'link': fields.String(required=True, description="Hyperlink to this file"),
        }
    )

    ns = api.namespace('flight_file',
        description='A flight can have several files associated with it (GPS traces, photos etc.)\nEach file has a type to provide context and behaviour for it.'
    )

    FlightFilesResource = post_get_resource(ns, api, "/", FlightFile, flight_file_create_model)
    FlightFileResource = get_put_delete_resource(
        ns, api, '/<int:id>', ('id', 'The flight file ID'), FlightFile, flight_file_edit_model)
