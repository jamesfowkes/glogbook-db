import json
import datetime

from flask import request
from flask_restx import Resource, fields

from app.models.flight import Flight
from app.models.flight_note import FlightNote
from app.models.flight_file import FlightFile
from app.models.enumerations import Filetype

from app.blueprints.api_resource import post_get_resource, get_put_delete_resource

def init_api(api):

    flight_edit_model = api.model("Flight", {
        'launch_dt': fields.DateTime(description="Date/time of launch"),
        'duration': fields.Integer(description="Flight duration in minutes"),

        'launch_height': fields.Integer(description="Height of launch/release in feet"),
        'max_height': fields.Integer(description="Maximum height acheived in feet"),
        'km_flown': fields.Integer(description="Distance flown"),
        'instructor': fields.String(description="Name of instructor"),

        'pilot_capacity_id': fields.Integer(description='ID of pilot capacity'),
        'glider_id': fields.Integer(description='ID of glider'),
        'location_id': fields.Integer(description='ID of location'),
        'launch_type_id': fields.String(description='ID of launch type')
        }
    )

    flight_create_model = api.model("FlightCreate", {
        'launch_dt': fields.DateTime(required=True, description="Date/time of launch"),
        'duration': fields.Integer(required=True, description="Flight duration in minutes"),

        'launch_height': fields.Integer(required=False, description="Height of launch/release in feet"),
        'max_height': fields.Integer(required=False, description="Maximum height acheived in feet"),
        'km_flown': fields.Integer(required=False, description="Distance flown"),
        'instructor': fields.String(required=False, description="Name of instructor"),

        'pilot_capacity_id': fields.Integer(required=True, description='ID of pilot capacity'),
        'glider_id': fields.Integer(required=True, description='ID of glider'),
        'location_id': fields.Integer(required=True, description='ID of location'),
        'launch_type_id': fields.String(required=True, description='ID of launch type')
        }
    )

    ns = api.namespace('flight', description='A flight is a single launch-to-landing in a glider or motorglider.')
    FlightsResource = post_get_resource(ns, api, "/", Flight, flight_create_model)
    FlightResource = get_put_delete_resource(
        ns, api, '/<int:id>', ('id', 'The flight ID'), Flight, flight_edit_model)

