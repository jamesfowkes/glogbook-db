import datetime
import time
from typing import OrderedDict
import click
import dateparser
import sqlalchemy
from tabulate import tabulate

import pyttsx3
engine = pyttsx3.init("sapi5")

from app.models.flight import Flight
from app.models.glider import Glider
from app.models.enumerations import LaunchType, PilotCapacity

from app.database import db
from app.models.location import Location
from app.utils import mins_to_hhmm

def datestr_to_date(date: str | datetime.datetime):

    # Date parser is pretty good, but to make sure it parser the year correctly,
    # add a "20" onto the front as aviation dates start with the year
    if type(date) is str:
        if not date.startswith("20"):
            date  = "20" + date
        
        return dateparser.parse(date)
    else:
        return date
    
def create_flight_from_cli_params(date, glider_id, location, launch_type, pilot_capacity, durations, launch_height=None, max_height=None, kms_flown=None, ins=None):
    # This CLI command is a complicated one. It needs to:
    #   1. Parse the date
    #   2. Find the glider and quit if not found
    #   3. Find the location (accept shorthand) and quit if there is ambiguity or not found
    #   4. Split the durations and create a flight for each one
    
    date = datestr_to_date(date)

    # Then get the glider
    glider = Glider.get_by_identifier(glider_id)

    if glider is None:
        print(f"Glider {glider_id} not found")
        return None

    # Then get the location, allowing partial matches (e.g. 'Rat' for Rattlesden)
    locations = Location.find(location, partial=True)
    if len(locations) > 1:
        print(f"Multiple locations found for {location}")
        return None
    if len(locations) == 0:
        print(f"No location found for {location}")
        return None
    
    location = locations[0] # find() returns a list

    # Get launch type and pilot capacity
    lt = LaunchType.get(launch_type)
    if lt is None:
        print(f"No launch type '{launch_type}' found")
        return None

    pcap = PilotCapacity.get_by_shortcode(pilot_capacity)
    if pcap is None:
        print(f"No pilot capacity '{pilot_capacity}' found")
        return None

    if not glider.glider_model.two_seater and pcap.is_P2():
        print(f"Cannot assign P2 flight to this glider")
        return None

    # Durations can be specified per-flight, semicolon-seperated, in ether mm or hh:mm format
    durations_ints = []
    for duration in durations:
        if ":" in duration:
            h, m = [int(part) for part in duration.split(":")]
            m = m + (h*60)
        else:
            m = int(duration)

        durations_ints.append(m)

    optional_args = {}
    if launch_height:
        if len(durations) > 0:
            print("Cannot supply launch height for multiple flights.")
            return None
        optional_args["launch_height"] = int(launch_height)
    else:
        launch_height = 0

    if max_height:
        if len(durations) > 0:
            print("Cannot supply maximum height for multiple flights.")
            return None
        optional_args["max_height"] = int(max_height)

    if kms_flown:
        if len(durations) > 0:
            print("Cannot supply kms flown for multiple flights.")
            return None

        optional_args["kms_flown"] = int(kms_flown)

    if ins:
        optional_args["instructor"] = ins

    flight_ids = []
    for duration in durations_ints:
        flight = Flight(
            launch_dt=date,
            duration=duration,
            launch_height=launch_height,
            pilot_capacity_id=pcap.pilot_capacity_id,
            glider_id = glider.glider_id,
            location_id=location.location_id,
            launch_type_id=lt.launch_type_id,
            **optional_args
        )
        db.session.add(flight)
        db.session.flush()
        db.session.commit()

        flight_ids.append(flight.flight_id)
    
    return flight_ids

def init_cli(app):
    @app.cli.command("flight_cli")
    def cli_interactive():
        try:
            last_entries = {
                "date": None, "glider_id": None, "location": None, "launch_type": None, "pilot_capacity": None
            }

            while True:
                next_flight_input = input("Enter next flight params: ")
                flight_ids = None
                if next_flight_input.count(",") == 5:
                    date, glider_id, location, launch_type, pilot_capacity, durations = next_flight_input.split(",")
                    ins = None
                elif next_flight_input.count(",") == 6:
                    date, glider_id, location, launch_type, pilot_capacity, durations, ins = next_flight_input.split(",")
                else:
                    print(f"'{next_flight_input}' not a valid entry.")
                    continue
        
                if date.startswith("+"):
                    last_date = datestr_to_date(last_entries["date"])
                    delta_days = int(date[1:])
                    date = last_date + datetime.timedelta(days=delta_days)
                elif date == "-":
                    date = last_entries["date"]
                else:
                    date = date

                if (last_entries["date"] is not None) and (datestr_to_date(date) < datestr_to_date(last_entries["date"])):
                    proceed = input(f"Entered date {date} is before last date {last_entries['date']}, are you sure (Y/N)?")
                    if proceed.upper() != "Y":
                        continue

                glider_id = glider_id if glider_id != "-" else last_entries["glider_id"]
                location = location if location != "-" else last_entries["location"]
                launch_type = launch_type if launch_type != "-" else last_entries["launch_type"]
                pilot_capacity = pilot_capacity if pilot_capacity != "-" else last_entries["pilot_capacity"]

                durations=durations.split(";")
                flight_ids = create_flight_from_cli_params(date, glider_id, location, launch_type, pilot_capacity, durations, ins=ins)
                
                if flight_ids:
                    last_entries["date"] = date
                    last_entries["glider_id"] = glider_id
                    last_entries["location"] = location
                    last_entries["launch_type"] = launch_type
                    last_entries["pilot_capacity"] = pilot_capacity
                    print("Created flight IDs: " + ",".join([str(fid) for fid in flight_ids]))

        except KeyboardInterrupt:
            pass

    @app.cli.command("create_flights")
    @click.argument("date")
    @click.argument("glider_id")
    @click.argument("location")
    @click.argument("launch_type")
    @click.argument("pilot_capacity")
    @click.argument("durations")
    @click.option("--launch_height")
    @click.option("--max_height")
    @click.option("--kms_flown")
    @click.option("--ins")
    def cli_create_flights(date, glider_id, location, launch_type, pilot_capacity, durations, launch_height, max_height, kms_flown, ins):
        durations = durations.split(",")
        flight_ids = create_flight_from_cli_params(date, glider_id, location, launch_type, pilot_capacity, durations, launch_height, max_height, kms_flown, ins)
        print("Created flight IDs: " + ",".join([str(fid) for fid in flight_ids]))

    @app.cli.command("list_flights")
    @click.option("--mrf/--lrf")
    @click.option("--from_id")
    @click.option("--to_id")
    @click.option("--talk",  is_flag=True)
    
    def cli_list_flights(mrf, from_id, to_id, talk):
        data = []
        tts_data = []
        data_by_flight_type = OrderedDict([
            ("P1", [0, 0]),
            ("P2 Student", [0, 0]),
            ("P1 Instructor", [0, 0]),
            ("P2 Student Instructor", [0, 0]),
            ("P1 Dual", [0, 0]),
            ("P2 Dual", [0, 0]),
            ("TMG", [0, 0]),
            ("Total", [0, 0])]
        )

        if from_id or to_id:
            flight_id_start = int(from_id) if from_id else 1
            flight_id_end = int(to_id) if to_id else db.session.query(sqlalchemy.func.max(Flight.flight_id))
            flights = db.session.execute(db.select(Flight).where((Flight.flight_id >= flight_id_start) & (Flight.flight_id <= flight_id_end)).order_by(Flight.flight_id.asc())).scalars()
        else:
            if mrf:
                flights = Flight.query.order_by(Flight.flight_id.desc()).all()
            else:
                flights = Flight.query.order_by(Flight.flight_id.asc()).all()

        for flight in flights:
            data.append([
                flight.flight_id, flight.launch_dt.date(), str(flight.glider), flight.location.name,
                flight.pilot_capacity.text, flight.duration_hhmm(),
                flight.launch_type_with_height(),
                flight.instructor,
                flight.km_flown if flight.km_flown > 0 else ""
            ])

            if flight.launch_type_id in ["A", "W", "B", "SL"]:
                data_by_flight_type[flight.pilot_capacity.text][0] += flight.duration
                data_by_flight_type[flight.pilot_capacity.text][1] += 1
            else:
                data_by_flight_type["TMG"][0] += flight.duration
                data_by_flight_type["TMG"][1] += 1

            data_by_flight_type["Total"][0] += flight.duration
            data_by_flight_type["Total"][1] += 1

            if talk:
                tts_data.append(f"Flight {flight.flight_id}, {flight.pilot_capacity.text}, {flight.duration}")

        headers = ["S/N", "Date", "Aircraft", "Location", "Capacity", "Duration", "Launch", "Instructor", "Kms Flown"]
        print(tabulate(data, headers=headers))
        print()
        data_times_by_capacity = OrderedDict()
        for k in data_by_flight_type:
            if data_by_flight_type[k][0] > 0:
                data_times_by_capacity[k] = [mins_to_hhmm(data_by_flight_type[k][0]) + " (" + str(data_by_flight_type[k][1]) + ")"]

        print(tabulate(data_times_by_capacity, headers=data_times_by_capacity.keys()))

        if talk:
            for tts_datum in tts_data:
                engine.say(tts_datum)
                engine.runAndWait()
                time.sleep(0.3)

    @app.cli.command("renumber_flights")
    @click.argument("start_id")
    @click.argument("relative_offset")
    def cli_renumber_flights(start_id, relative_offset):
        start_id = int(start_id)
        relative_offset = int(relative_offset)

        if relative_offset > 0:
            flights = db.session.execute(db.select(Flight).order_by(Flight.flight_id.desc())).scalars()
        else:
            flights = db.session.execute(db.select(Flight).order_by(Flight.flight_id.asc())).scalars()

        for flight in flights:
            if flight.flight_id >= start_id:
                flight.flight_id += relative_offset

            db.session.flush()
            db.session.commit()

    @app.cli.command("reset_seq")
    @click.option("--start_id")
    def cli_renumber_flights(start_id):
        if start_id is None:
            start_id = db.session.query(sqlalchemy.func.max(Flight.flight_id)).scalar() + 1
        try:
            db.session.execute(sqlalchemy.text(f'ALTER SEQUENCE "flights_flight_id_seq" RESTART WITH {start_id}'))
            db.session.commit()
            print(f"Next sequence number: {start_id}")
        except:
            raise