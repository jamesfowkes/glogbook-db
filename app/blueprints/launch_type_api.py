import json
import decimal

from flask import request
from flask_restx import Resource, fields

from app.blueprints.api_resource import post_get_resource, get_put_delete_resource
from app.models.enumerations import LaunchType

def init_api(api):

    launch_type_edit_model = api.model("LaunchType", {
        'launch_type_id': fields.Integer(readonly=True, description="Launch type ID (W, A, etc.)"),
        'description': fields.String(description="Description of launch type")
        }
    )

    launch_type_create_model = api.model("LaunchTypeCreate", {
        'description': fields.String(required=True, description="Description")
        }
    )

    ns = api.namespace('launch_type', description='Launch types are winch, aerotow etc.')

    LaunchTypesResource = post_get_resource(ns, api, "/", LaunchType, launch_type_create_model)
    LaunchTypeResource = get_put_delete_resource(
        ns, api, '/<id>', ('id', 'The launch type ID'), LaunchType, launch_type_edit_model)
