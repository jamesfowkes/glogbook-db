from flask_restx import fields

from app.blueprints.api_resource import post_get_resource, get_put_delete_resource
from app.models.location import Location

def init_api(api):

    location_edit_model = api.model("Location", {
        'name': fields.String(readonly=True, description="Name of location"),
        'description': fields.String(description="Description of location"),
        'lat': fields.Fixed(decimals=6, description="Latitude of location"),
        'lon': fields.Fixed(decimals=6, description="longitude of location"),
        }
    )

    location_create_model = api.model("LocationCreate", {
        'name': fields.String(required=True, description="Name of location"),
        'description': fields.String(required=True, description="Description of location"),
        'lat': fields.Fixed(required=True, decimals=6, description="Latitude of location"),
        'lon': fields.Fixed(required=True, decimals=6, description="longitude of location"),
        }
    )

    ns = api.namespace('location', description='Each flight is associated with a launch location.')

    LocationsResource = post_get_resource(ns, api, "/", Location, location_create_model)
    LocationResource = get_put_delete_resource(
        ns, api, '/<int:id>', ('id', 'The location ID'), Location, location_edit_model)
