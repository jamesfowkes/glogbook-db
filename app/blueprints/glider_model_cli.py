import click
from app.models.glider_model import GliderModel

from app.database import db

def init_cli(app):
    @app.cli.command("create_glider_model")
    @click.argument("name")
    @click.argument("manufacturer")
    @click.argument("description")
    @click.option("--two_seater/--single_seater", default=True)
    def cli_create_glider_model(**kwargs):
        
        data = {
            "name": str(kwargs["name"]),
            "manufacturer": str(kwargs["manufacturer"]),
            "description": str(kwargs["description"]),
            "two_seater": kwargs["two_seater"]
        }
        gt = GliderModel(**data)
        gt.save()

    @app.cli.command("list_glider_models")
    def cli_list_glider_models():
        glider_models = db.session.execute(db.select(GliderModel)).scalars()
        for glider_model in glider_models:
            print(f"{glider_model.name}, {glider_model.manufacturer}, {glider_model.description}")
