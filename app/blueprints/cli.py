from flask import Blueprint

from app.blueprints.glider_cli import init_cli as glider_cli_init_cli
from app.blueprints.location_cli import init_cli as location_cli_init_cli
from app.blueprints.flight_cli import init_cli as flight_cli_init_cli
#from app.blueprints.pilot_cap_cli import init_cli as pilot_cap_cli_init_cli
#from app.blueprints.launch_type_cli import init_cli as launch_type_cli_init_cli
#from app.blueprints.filetype_cli import init_cli as filetype_cli_init_cli
from app.blueprints.glider_model_cli import init_cli as glider_model_cli_init_cli
#from app.blueprints.flight_file_cli import init_cli as flight_file_cli_init_cli
#from app.blueprints.flight_note_cli import init_cli as flight_note_cli_init_cli

blueprint = Blueprint('cli', __name__)

def init_app(app):
    app.register_blueprint(blueprint)

    glider_cli_init_cli(app)
    location_cli_init_cli(app)
    flight_cli_init_cli(app)
    #pilot_cap_cli_init_cli(app)
    #launch_type_cli_init_cli(app)
    #filetype_cli_init_cli(app)
    glider_model_cli_init_cli(app)
    #flight_note_cli_init_cli(app)
    #flight_file_cli_init_cli(app)
