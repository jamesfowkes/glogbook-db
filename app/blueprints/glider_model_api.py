import json
import decimal

from flask import request
from flask_restx import Resource, fields

from app.blueprints.api_resource import post_get_resource, get_put_delete_resource
from app.models.glider_model import GliderModel

def init_api(api):

    glider_model_edit_model = api.model("GliderModel", {
        'glider_model_id': fields.Integer(readonly=True, description="Glider type ID"),
        'name': fields.String(description="Name of glider"),
        'manufacturer': fields.String(description="Manufacturer of glider"),
        'description': fields.String(description="Basic description of glider"),
        'two_seater': fields.Boolean(description="Is the glider a two_seater?")
        }
    )

    glider_model_create_model = api.model("GliderModelCreate", {
        'name': fields.String(required=True, description="Name of glider"),
        'manufacturer': fields.String(required=True, description="Manufacturer of glider"),
        'description': fields.String(required=True, description="Basic description of glider"),
        'two_seater': fields.Boolean(required=True, description="Is the glider a two_seater?")
        }
    )

    ns = api.namespace('glider_model', description='A glider type is a specific make, model and variant of glider.')

    GliderModelsResource = post_get_resource(ns, api, "/", GliderModel, glider_model_create_model)
    GliderModelResource = get_put_delete_resource(
        ns, api, '/<int:id>', ('id', 'The glider type ID'), GliderModel, glider_model_edit_model)
