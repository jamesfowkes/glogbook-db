import json

from flask import Blueprint
from flask_restx import Api

from app.blueprints.glider_api import init_api as glider_api_init_api
from app.blueprints.location_api import init_api as location_api_init_api
from app.blueprints.flight_api import init_api as flight_api_init_api
from app.blueprints.pilot_cap_api import init_api as pilot_cap_api_init_api
from app.blueprints.launch_type_api import init_api as launch_type_api_init_api
from app.blueprints.filetype_api import init_api as filetype_api_init_api
from app.blueprints.glider_model_api import init_api as glider_model_api_init_api
from app.blueprints.glider_identifier_api import init_api as glider_identifier_api_init_api
from app.blueprints.flight_file_api import init_api as flight_file_api_init_api
from app.blueprints.flight_note_api import init_api as flight_note_api_init_api

api = Api(version='1.0', title='Gliding Logbook API', description='Provides access to gliding logbook database', validate=True)
blueprint = Blueprint('api', __name__, url_prefix='/api')

def init_app(app):
    api.init_app(blueprint)
    app.register_blueprint(blueprint)

    glider_api_init_api(api)
    location_api_init_api(api)
    flight_api_init_api(api)
    pilot_cap_api_init_api(api)
    launch_type_api_init_api(api)
    filetype_api_init_api(api)
    glider_model_api_init_api(api)
    glider_identifier_api_init_api(api)
    flight_note_api_init_api(api)
    flight_file_api_init_api(api)
