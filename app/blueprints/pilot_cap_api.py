import json
import decimal

from flask import request
from flask_restx import Resource, fields

from app.blueprints.api_resource import post_get_resource, get_put_delete_resource
from app.models.enumerations import PilotCapacity

def init_api(api):

    pilot_capacity_edit_model = api.model("PilotCapacity", {
        'pilot_capacity_id': fields.Integer(readonly=True, description="Unique pilot capacity ID"),
        'text': fields.String(readonly=True, description="Short identifier"),
        'description': fields.String(description="Long description")
        }
    )

    pilot_capacity_create_model = api.model("PilotCapacityCreate", {
        'text': fields.String(required=True, description="Short identifier"),
        'description': fields.String(required=True, description="Long description")
        }
    )

    ns = api.namespace('pilot_capacity', description='Each flight has a pilot capacity associated with it.\n')

    PilotCapacitiesResource = post_get_resource(ns, api, "/", PilotCapacity, pilot_capacity_create_model)
    PilotCapacityResource = get_put_delete_resource(
        ns, api, '/<int:id>', ('id', 'The pilot capacity ID'), PilotCapacity, pilot_capacity_edit_model)
