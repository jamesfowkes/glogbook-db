import json

from flask import request
from flask_restx import Resource, fields

from app.blueprints.api_resource import post_get_resource, get_put_delete_resource
from app.models.flight_note import FlightNote

def init_api(api):

    flight_note_edit_model = api.model("FlightNote", {
        'text': fields.String(description="Text of this note"),
        }
    )

    flight_note_create_model = api.model("FlightNoteCreate", {
        'flight_id': fields.Integer(required=True, description="Associated flight ID"),
        'text': fields.String(required=True, description="Text of this note"),
        }
    )

    ns = api.namespace('flight_note', description='Each flight can have several notes (text) associated with it.')

    FlightNotesResource = post_get_resource(ns, api, "/", FlightNote, flight_note_create_model)
    FlightNoteResource = get_put_delete_resource(
        ns, api, '/<int:id>', ('id', 'The flight note ID'), FlightNote, flight_note_edit_model)
