import os
import logging
import logging.handlers
import datetime

from pathlib import Path

from flask import Flask
from json import JSONEncoder

from flask_migrate import Migrate

APP_PATH = Path(__file__).parent

class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, datetime.datetime):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)

def create_app(config_class=None):

	app = Flask(__name__)

	app.json_encoder = CustomJSONEncoder

	if config_class:
		app.config.from_object(config_class)
	else:
		app.config.from_object(os.environ["CONFIG_CLASS"])

	log_level = app.config["DEBUG"]
	logfile = app.config["LOGFILE"]

	formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')
	handler = logging.handlers.RotatingFileHandler(logfile, maxBytes=1024*1204, backupCount=1)
	handler.setFormatter(formatter)
	handler.setLevel(log_level)
	root_logger = logging.getLogger()
	root_logger.addHandler(handler)

	from app import database 
	db = database.init_app(app)

	# Initialise blueprints after the views have been imported to correctly register endpoints
	from app.blueprints import api
	from app.blueprints import cli
     
	api.init_app(app)
	cli.init_app(app)
 
	migrate = Migrate(app, db)

	return app
	