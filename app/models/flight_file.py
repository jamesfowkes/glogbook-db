import sqlalchemy
from flask import jsonify

from app.database import db
from app.models import BaseModel

class FlightFile(db.Model, BaseModel):

    __tablename__ = "flight_files"

    flight_id = db.Column(db.Integer, db.ForeignKey("flights.flight_id"), nullable=False)
    flight_file_id = db.Column(db.Integer, db.Sequence("file_id_seq"))
    filetype_id = db.Column(db.Integer, db.ForeignKey("filetypes.filetype_id"), nullable=False)
    link = db.Column(db.String(1024))

    __table_args__ = (
        db.PrimaryKeyConstraint("flight_id", "flight_file_id"),
        {}
    )
    
    flight = db.relationship("Flight", back_populates="flight_files")
    filetype = db.relationship("Filetype", back_populates="files")

    unique_id_key = "flight_file_id"

    def data(self):
        return {
            "flight_id": self.flight_id,
            "flight_file_id": self.flight_file_id,
            "filetype_id": self.filetype_id,
            "link": self.link,   
        }

    def edit(self, data):

        if "filetype_id" in data:
            self.filetype_id = data["filetype_id"]
        if "link" in data:
            self.link = data["link"]

        self.save()

        return self, ""
