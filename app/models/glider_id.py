from flask import jsonify

from app.database import db
from app.models import BaseModel

class GliderIdentifier(db.Model, BaseModel):

    __tablename__ = "glider_identifiers"

    glider_id = db.Column(db.Integer, db.ForeignKey("gliders.glider_id"), nullable=False, primary_key=True)
    glider_identifier_id = db.Column(db.Integer, db.Sequence("glider_id_seq"))
    glider_identifier_type_id = db.Column(db.Integer,  db.ForeignKey("identifier_types.identifier_type_id"), nullable=False)

    identifier = db.Column(db.String(10), primary_key=True)

    __table_args__ = (
        db.Index('unique_reg_when_type_is_reg', identifier, unique=True, postgresql_where=(glider_identifier_type_id == 1)),
        db.Index('unique_trigraph_when_type_is_trigraph', identifier, unique=True, postgresql_where=(glider_identifier_type_id == 2)),
    )

    identifier_type = db.relationship("IdentifierType", back_populates="glider_identifiers")
    glider = db.relationship("Glider", back_populates="identifiers")

    unique_id_key = "glider_id_seq"

    def data(self):
        return {
            'glider_id': self.glider_id,
            'identifier_type': self.identifier_type,
            'glider_identifier_id': self.glider_identifier_id,
            'identifier': self.identifier
        }

    def edit(self, data):

        self.save()

        return self, ""

    def __repr__(self):
        return "<GliderIdentifier: {} ({})>".format(self.identifier, self.identifier_type.description)
