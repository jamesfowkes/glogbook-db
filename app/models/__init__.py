import logging
import json
import decimal
import sqlalchemy

from collections import OrderedDict

from flask import jsonify

from app.database import db

logger = logging.getLogger(__name__)

class BaseModel:

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def __getitem__(self, item):
        return self.data()[item]

    @classmethod
    def request_all(cls):
        return [res.data() for res in cls.query.all()]

    @classmethod
    def request_by_id(cls, id):
        filter_args = {cls.unique_id_key:id}
        return cls.query.filter_by(**filter_args).one()

    @classmethod
    def request_delete(cls, id):
        filter_args = {cls.unique_id_key:id}
        obj = cls.query.filter_by(**filter_args).first()
        
        result = None

        if obj is not None:
            try:
                result = obj.data()
                obj.delete()
            except sqlalchemy.exc.IntegrityError as e:
                result = False

        return result

    @classmethod
    def request_edit(cls, id, data):
        filter_args = {cls.unique_id_key:id}
        obj = cls.query.filter_by(**filter_args).first()
        return obj.edit(data)

    @classmethod
    def request_create(cls, request):

        new_obj = cls(**request)
        try:
            new_obj.save()
            response = True, new_obj.data()
        except sqlalchemy.exc.IntegrityError as e:
            response = False, str(e)

        return response

    @classmethod
    def request_query(self, **kwargs):
        return [res.data() for res in cls.query.filter_by(**kwargs)]
