from flask import jsonify

from app.database import db
from app.models import BaseModel

class GliderModel(db.Model, BaseModel):

    __tablename__ = "glider_models"

    glider_model_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    manufacturer = db.Column(db.String(64), nullable=False)
    description = db.Column(db.String(255), nullable=False)
    two_seater = db.Column(db.Boolean(255), nullable=False)

    gliders = db.relationship("Glider", back_populates="glider_model")

    unique_id_key = "glider_model_id"

    def data(self):
        return {
            'glider_model_id': self.glider_model_id,
            'name': self.name,
            'manufacturer': self.manufacturer,
            'description': self.description, 
            "two_seater": self.two_seater
        }

    def edit(self, data):
        if "name" in data:
            self.name = data["name"]

        if "manufacturer" in data:
            self.manufacturer = data["manufacturer"]

        if "description" in data:
            self.description = data["description"]

        if "two_seater" in data:
            self.two_seater = data["two_seater"] == "True"

        self.save()

        return self, ""

    def __repr__(self):
        return "<GliderModel: {}, {}, {}, {}>".format(self.name, self.manufacturer, self.description, self.two_seater)
