from datetime import datetime, timedelta

import sqlalchemy
from flask import jsonify

from app.database import db
from app.models import BaseModel

from app.models.flight_note import FlightNote
from app.models.flight_file import FlightFile

from app.models.enumerations import PilotCapacity, LaunchType
from app.utils import mins_to_hhmm

class Flight(db.Model, BaseModel):

    __tablename__ = "flights"

    flight_id = db.Column(db.Integer, primary_key=True)
    launch_dt = db.Column(db.DateTime, nullable=False)
    duration = db.Column(db.Integer, nullable=False)
    pilot_capacity_id = db.Column(db.Integer,  db.ForeignKey("pilot_capacities.pilot_capacity_id"), nullable=False)
    glider_id = db.Column(db.Integer,  db.ForeignKey("gliders.glider_id"), nullable=False)
    location_id = db.Column(db.Integer,  db.ForeignKey("locations.location_id"), nullable=False)
    launch_type_id = db.Column(db.String(2),  db.ForeignKey("launch_types.launch_type_id"), nullable=False)

    launch_height = db.Column(db.Integer, nullable=False)
    max_height = db.Column(db.Integer, nullable=False)
    km_flown = db.Column(db.Integer, nullable=False)
    instructor = db.Column(db.String(64))

    flight_notes = db.relationship("FlightNote", back_populates="flight", cascade="all, delete, delete-orphan")
    flight_files = db.relationship("FlightFile", back_populates="flight", cascade="all, delete, delete-orphan")
    pilot_capacity = db.relationship("PilotCapacity", back_populates="flights")
    glider = db.relationship("Glider", back_populates="flights")
    location = db.relationship("Location", back_populates="flights")
    launch_type = db.relationship("LaunchType", back_populates="flights")

    unique_id_key = "flight_id"
    
    def __init__(self, launch_dt, duration, pilot_capacity_id, glider_id, location_id, launch_type_id,
        launch_height=0, max_height=0, km_flown=0, instructor=""):

        if type(launch_dt) is str:
            launch_dt = datetime.fromisoformat(launch_dt)
        self.launch_dt = launch_dt

        self.duration = duration
        self.pilot_capacity_id = pilot_capacity_id
        self.glider_id = glider_id
        self.location_id = location_id
        self.launch_type_id = launch_type_id
        self.launch_height = launch_height
        self.max_height = max_height
        self.km_flown = km_flown
        self.instructor = instructor

    def data(self):
        return {
            "flight_id": self.flight_id,
            "launch_dt": self.launch_dt.isoformat(),
            "landing_dt": self.landing_dt().isoformat(),
            "duration": self.duration,
            "pilot_capacity_id": self.pilot_capacity_id,
            "pilot_capacity": self.pilot_capacity.text,
            "glider_id": self.glider_id,
            "glider": self.glider.identifier(),
            "location_id": self.location_id,
            "location": self.location.name,
            "launch_type_id": self.launch_type_id,
            "launch_type": self.launch_type.description,
            "launch_height": self.launch_height,
            "max_height": self.max_height,
            "km_flown": self.km_flown,
            "instructor": self.instructor,
            "notes": [note.text for note in self.flight_notes]
        }

    def duration_hhmm(self):
        return mins_to_hhmm(self.duration)

    def launch_type_with_height(self):
        if self.launch_type.description == "Aerotow" and self.launch_height > 0:
            return f"Aerotow ({self.launch_height}ft)"
        else:
            return self.launch_type.description

    def landing_dt(self):
        return self.launch_dt + timedelta(seconds=self.duration*60)

    def edit(self, data):

        if "launch_dt" in data:
            self.launch_dt = datetime.fromisoformat(data["launch_dt"])

        if "duration" in data:
            self.duration = data["duration"]

        if "pilot_capacity_id" in data:
            self.pilot_capacity_id = data["pilot_capacity_id"]

        if "glider_id" in data:
            self.glider_id = data["glider_id"]

        if "location_id" in data:
            self.location_id = data["location_id"]

        if "launch_type_id" in data:
            self.launch_type_id = data["launch_type_id"]

        if "launch_height" in data:
            self.launch_height = data["launch_height"]

        if "max_height" in data:
            self.max_height = data["max_height"]

        if "km_flown" in data:
            self.km_flown = data["km_flown"]

        if "instructor" in data:
            self.instructor = data["instructor"]

        self.save()

        return self, ""
