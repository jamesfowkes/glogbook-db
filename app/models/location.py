import json
from decimal import Decimal

import sqlalchemy
from flask import jsonify

from app.database import db
from app.models import BaseModel

class Location(db.Model, BaseModel):

    __tablename__ = "locations"

    location_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    description = db.Column(db.String(255))
    lat = db.Column(db.Numeric)
    lon = db.Column(db.Numeric)

    flights = db.relationship("Flight", back_populates="location")

    unique_id_key = "location_id"
   
    def data(self):
        return {
            'location_id': self.location_id,
            'name': self.name,
            'description': self.description,
            'lat': float(self.lat),
            'lon': float(self.lon)
        }

    def edit(self, data):

        if "name" in data:
            self.name = data["name"]

        if "description" in data:
            self.description = data["description"]

        if "lat" in data:
            self.lat = data["lat"]

        if "lon" in data:
            self.lon = data["lon"]

        self.save()

        return self, ""

    def __format__(self, format_spec):
        return f"{self.name}, {self.description}"

    @classmethod
    def find(cls, name, partial):
        if partial:
            locations = db.session.execute(db.select(Location).where(Location.name.startswith(name))).scalars().all()
        else:
            locations = db.session.execute(db.select(Location).where(Location.name==name)).scalars().all()

        return locations
