from app.database import db
from app.models import BaseModel
from app.models.glider_id import GliderIdentifier

class Glider(db.Model, BaseModel):

    __tablename__ = "gliders"

    glider_id = db.Column(db.Integer, primary_key=True)
    glider_model_id = db.Column(db.Integer, db.ForeignKey("glider_models.glider_model_id"), nullable=False)

    glider_model = db.relationship("GliderModel", back_populates="gliders")
    flights = db.relationship("Flight", back_populates="glider")
    identifiers = db.relationship("GliderIdentifier", back_populates="glider")

    unique_id_key = "glider_id"

    def __init__(self, glider_model_id):
        self.glider_model_id = glider_model_id

    def data(self):
        return {
            'glider_id': self.glider_id,
            'glider_model_id': self.glider_model_id,
            'glider_model': self.glider_model.name
        }

    def edit(self, data):

        if "self_type_id" in data:
            self.glider_model_id = data["glider_model_id"] == "True"

        self.save()

        return self, ""

    def all_identifiers(self, join=False):
        identifiers = [id.identifier for id in self.identifiers]

        return ", ".join(identifiers) if join else identifiers

    def __str__(self):
        return f"{self.primary_identifier()} ({self.glider_model.name})"

    def trigraph(self):
        for identifier in self.identifiers:
            if identifier.identifier_type.description == "BGA trigraph":
                return identifier.identifier
        return None

    def comp_no(self):
        comp_nos = []
        for identifier in self.identifiers:
            if identifier.identifier_type.description == "Competition Number":
                comp_nos.append(identifier.identifier)

        return comp_nos[-1] if len(comp_nos) else None
    
    def greg(self):
        for identifier in self.identifiers:
            if identifier.identifier_type.description == "Registration":
                return identifier.identifier
        return None

    def primary_identifier(self):
        comp_no, trigraph, registration = self.comp_no(), self. trigraph(), self.greg()
        if comp_no:
            return comp_no
        
        if trigraph:
            return trigraph
        
        if registration:
            return registration
        
        return None

    @classmethod
    def get_by_identifier(cls, identifier):
        identifier = db.session.execute(db.select(GliderIdentifier).where(GliderIdentifier.identifier==identifier)).scalar()
        return identifier.glider if identifier else None