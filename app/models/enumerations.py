import sqlalchemy
from flask import jsonify

from app.database import db
from app.models import BaseModel

class PilotCapacity(db.Model, BaseModel):

    __tablename__ = "pilot_capacities"

    pilot_capacity_id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(32), nullable=False)
    description = db.Column(db.String(255), nullable=False)

    flights = db.relationship("Flight", back_populates="pilot_capacity")

    unique_id_key = "pilot_capacity_id"

    def data(self):
        return {
            'pilot_capacity_id': self.pilot_capacity_id,
            'text': self.text,
            'description': self.description
        }
        
    def edit(self, data):
        if "description" in data:
            self.description = data["description"]
        self.save()

        return self, ""

    def is_P2(self):
        return "P2" in self.text

    @classmethod
    def get_by_shortcode(cls, code):
        pcap_map = {
            "P1": "P1",
            "P2": "P2 Student",
            "P1I": "P1 Instructor",
            "P2I": "P2 Student Instructor",
            "P1D": "P1 Dual",
            "P2D": "P2 Dual"
        }

        try:
            text = pcap_map[code]
        except KeyError:
            return None

        return db.session.execute(db.select(PilotCapacity).where(PilotCapacity.text==text)).scalar_one()

class Filetype(db.Model, BaseModel):

    __tablename__ = "filetypes"

    filetype_id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(255), nullable=False)
    
    files = db.relationship("FlightFile", back_populates="filetype")

    unique_id_key = "filetype_id"

    def data(self):
        return {
            'filetype_id': self.filetype_id,
            'description': self.description
        }

    def edit(self, data):
        if "description" in data:
            self.description = data["description"]
        self.save()

        return self, ""

class LaunchType(db.Model, BaseModel):

    __tablename__ = "launch_types"

    launch_type_id = db.Column(db.String(2), primary_key=True)
    description = db.Column(db.String(255), nullable=False)
    
    flights = db.relationship("Flight", back_populates="launch_type")

    unique_id_key = "launch_type_id"

    def data(self):
        return {
            'launch_type_id': self.launch_type_id,
            'description': self.description
        }

    def edit(self, data):
        if "description" in data:
            self.description = data["description"]
        self.save()

        return self, ""
    
    @classmethod
    def get(cls, lt_id):
        try:
            return db.session.execute(db.select(LaunchType).where(LaunchType.launch_type_id==lt_id)).scalar_one()
        except sqlalchemy.exc.NoResultFound:
            return None

class IdentifierType(db.Model, BaseModel):

    __tablename__ = "identifier_types"

    identifier_type_id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(255), nullable=False)
    
    glider_identifiers = db.relationship("GliderIdentifier", back_populates="identifier_type")

    unique_id_key = "identifier_type_id"

    def data(self):
        return {
            'identifier_type_id': self.identifier_type_id,
            'description': self.description
        }

    def edit(self, data):
        if "description" in data:
            self.description = data["description"]
        self.save()

        return self, ""
