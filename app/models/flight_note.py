import sqlalchemy
from flask import jsonify

from app.database import db
from app.models import BaseModel

class FlightNote(db.Model, BaseModel):

    __tablename__ = "flight_notes"

    flight_id = db.Column(db.Integer, db.ForeignKey("flights.flight_id"), nullable=False)
    flight_note_id = db.Column(db.Integer, db.Sequence("note_id_seq"))
    text = db.Column(db.String(1024))

    __table_args__ = (
        db.PrimaryKeyConstraint("flight_id", "text"),
        {}
    )
    
    flight = db.relationship("Flight", back_populates="flight_notes")

    unique_id_key = "flight_note_id"

    def data(self):
        return {
            'flight_id': self.flight_id,
            'flight_note_id': self.flight_note_id,
            'text': self.text,
        }

    def edit(self, data):

        if "text" in data:
            self.text = data["text"]

        self.save()

        return self, ""
