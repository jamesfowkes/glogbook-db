import pytest
import os
import json

from collections import OrderedDict

from tests import JSONResponseHelper, UnitTestDataHelper, create_json_data
from tests.fixtures import app, app_client, gliders, glider_models

from app.models.glider_model import GliderModel

GLIDER_MODELS = [
    {"name": "ASK21", "manufacturer": "Alexander Schleicher","description": "Basic GRP trainer", "two_seater": True},
    {"name": "Discus", "manufacturer": "Schempp-Hirth", "description": "Standard Class", "two_seater": False}
]

ENDPOINT = '/api/glider_model/'

def create_test_glider_models(data_list):

    with app.app_context():
        glider_models = {}
        for data in data_list:
            glider_model = GliderModel(**data)
            glider_model.save()
            glider_models[glider_model.glider_model_id] = glider_model

    return UnitTestDataHelper(glider_models)

def create_glider_models_by_request(client, data_list):

    responses = []
    glider_model_ids = []
    for data in data_list:
        data_copy = data.copy()
        data_json = json.dumps(data_copy)
        response = client().post(ENDPOINT, data=data_json, headers={'content-type': 'application/json'}) 
        response = JSONResponseHelper(response)
        responses.append(response)
        glider_model_ids.append(response.json["glider_model_id"])

    return glider_model_ids, responses

class TestGliderModel:

    def test_glider_model_creation_valid_data(self, app_client):

        ### Action ###

        _, responses = create_glider_models_by_request(app_client, glider_modelS)
        
        ### Tests ###
        
        for resp, expected_data in zip(responses, glider_modelS):
            assert resp.status_code == 201
            resp.json.pop("glider_model_id")
            assert resp.json == expected_data

    def test_glider_model_creation_missing_data_all_fields(self, app_client):

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=json.dumps({}), headers={'content-type': 'application/json'}))

        ### Tests ###

        assert resp.status_code == 400
        
        for key in glider_modelS[0].keys():
            assert key in resp.json["errors"]

    @pytest.mark.parametrize("missing_field", ['name', 'manufacturer', 'description', 'two_seater'])
    def test_glider_model_creation_missing_data_one_field(self, app_client, missing_field):

        ### Setup ###

        invalid_data = create_json_data(glider_modelS[0], missing_fields=[missing_field])

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'}))

        ### Tests ###
        
        assert resp.status_code == 400
        assert missing_field in resp.json["errors"]

    @pytest.mark.parametrize("present_field", ['name', 'manufacturer', 'description', 'two_seater'])
    def test_glider_model_creation_missing_data_several_fields(self, app_client, present_field):

        ### Setup ###
        missing_fields = ["name", "manufacturer", "description", "two_seater"]
        missing_fields.remove(present_field)
        invalid_data = create_json_data(glider_modelS[0], missing_fields=missing_fields)

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'}))

        ### Tests ###

        assert resp.status_code == 400
        for missing_field in missing_fields:
            assert missing_field in resp.json["errors"]

    @pytest.mark.parametrize("field, value",
        [
            ("name", 0),
            ("manufacturer", 0),
            ("description", 0),
            ("two_seater", "True")
        ]
    )
    def test_glider_model_creation_invalid_str_data(self, app_client, field, value):

        ### Setup ###

        invalid_data = create_json_data(glider_modelS[0], invalid_fields={field: value})

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'}))

        ### Tests ###

        assert resp.status_code == 400
        assert field in resp.json["errors"]
        assert str(value) in resp.json["errors"][field]

    def test_api_can_get_all_glider_models(self, app_client):

        ### Setup ###

        glider_models = create_test_glider_models(glider_modelS)

        ### Action ###

        resp = JSONResponseHelper(app_client().get(ENDPOINT))

        ### Tests ###

        assert resp.status_code == 200

        assert resp.json[0].pop("glider_model_id") == glider_models.ids[0]
        assert resp.json[1].pop("glider_model_id") == glider_models.ids[1]
        
        assert resp.json[0] == glider_modelS[0]
        assert resp.json[1] == glider_modelS[1]

    def test_api_can_get_glider_model_by_id(self, app_client):

        ### Setup ###

        glider_models = create_test_glider_models(glider_modelS)

        ### Action ###

        responses = []
        for glider_model_id in glider_models.ids:
            resp = JSONResponseHelper(app_client().get(ENDPOINT + str(glider_model_id)))
            responses.append(resp)

        ### Tests ###

        for response, expected_glider_model_id, expected_glider_model in zip(responses, glider_models.ids, glider_modelS): 
            assert response.status_code == 200
            assert response.json["glider_model_id"] == expected_glider_model_id
            assert response.json["name"] == expected_glider_model["name"]
            assert response.json["manufacturer"] == expected_glider_model["manufacturer"]
            assert response.json["description"] == expected_glider_model["description"]
            assert response.json["two_seater"] == expected_glider_model["two_seater"]

    def test_glider_model_can_be_edited_with_valid_data(self, app_client):

        ### Setup ###

        glider_models = create_test_glider_models(glider_modelS)
        glider_model_id = glider_models.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(glider_model_id), data=json.dumps({"name": "K21"}),
            headers={'content-type': 'application/json'}
        )

        ### Tests ###

        assert resp.status_code == 200

        resp = app_client().get(ENDPOINT + str(glider_model_id))
            
        assert resp.json["name"] == "K21"

    @pytest.mark.parametrize("field, value",
        [
            ("name", 0),
            ("manufacturer", 0),
            ("description", 0),
            ("two_seater", "True")
        ]
    )
    def test_glider_model_cannot_be_edited_with_invalid_data(self, app_client, field, value):
        
        ### Setup ###

        glider_models = create_test_glider_models(glider_modelS)
        glider_model_id = glider_models.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(glider_model_id), data=json.dumps({field:value}),
            headers={'content-type': 'application/json'}
        )

        ### Tests ###

        assert resp.status_code == 400

        assert field in resp.json["errors"]
        assert str(value) in resp.json["errors"][field]

        resp = app_client().get(ENDPOINT + str(glider_model_id))
        assert resp.json["name"] == glider_modelS[0]["name"]
        assert resp.json["manufacturer"] == glider_modelS[0]["manufacturer"]
        assert resp.json["description"] == glider_modelS[0]["description"]
        assert resp.json["two_seater"] == glider_modelS[0]["two_seater"]

    def test_glider_model_deletion(self, app_client):
        
        ### Setup ###

        glider_models = create_test_glider_models(glider_modelS)

        ### Action ###

        resp = app_client().delete(ENDPOINT + str(glider_models.ids[0]))

        ### Tests ###

        assert resp.status_code == 200

        result = app_client().get(ENDPOINT + str(glider_models.ids[0]))
        assert result.status_code == 404

        result = app_client().get(ENDPOINT + str(glider_models.ids[1]))
        assert result.status_code == 200

    def test_glider_model_cannot_be_deleted_with_associated_glider(self, app_client, gliders, glider_models):

        ### Action ###

        glider_model_id = gliders.data[0].glider_model_id
        resp = app_client().delete(ENDPOINT + str(glider_model_id))

        ### Tests ###

        assert resp.status_code == 400

        result = app_client().get(ENDPOINT + str(glider_model_id))
        assert result.status_code == 200
