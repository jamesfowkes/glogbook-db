import pytest
import os
import json

from datetime import datetime, timedelta

from collections import OrderedDict

from tests import JSONResponseHelper, UnitTestDataHelper, create_json_data
from tests.fixtures import app, app_client, flights, gliders, pilot_capacities, locations, launch_types
from tests.test_flight_file import create_test_flight_files, FLIGHT_FILES
from tests.test_flight_note import create_test_flight_notes, FLIGHT_NOTES

from app.models.glider import Glider
from app.models.flight import Flight

FLIGHTS = [
    {
        "launch_dt": "2020-07-31T12:00:00",
        "duration": 10,
        "launch_height": 1500,
        "max_height": 1500,
        "km_flown": 0,
        "instructor": "John Doe"
    },
    {
        "launch_dt": "2020-07-31T13:00:00",
        "duration": 20
    }
]

ENDPOINT = '/api/flight/'

def get_foreign_keys(index, pilot_capacities, gliders, locations, launch_types):
    pilot_capacity_id = pilot_capacities.ids[index]
    glider_id = gliders.ids[index]
    location_id = locations.ids[index]
    launch_type_id = launch_types.ids[index]

    return pilot_capacity_id, glider_id, location_id, launch_type_id

def create_flight_data(data, pilot_capacity_id, glider_id, location_id, launch_type_id):
    data_copy = data.copy()
    data_copy["pilot_capacity_id"] = pilot_capacity_id
    data_copy["glider_id"] = glider_id
    data_copy["location_id"] = location_id
    data_copy["launch_type_id"] = launch_type_id
    return data_copy

def create_test_flights(data_list, pilot_capacities, gliders, locations, launch_types):

    responses = []

    with app.app_context():
        flights = {}
        for data, pilot_capacity_id, glider_id, location_id, launch_type_id in zip(data_list, pilot_capacities.ids, gliders.ids, locations.ids, launch_types.ids):
            
            data = data.copy()

            data["pilot_capacity_id"] = pilot_capacity_id
            data["glider_id"] = glider_id
            data["location_id"] = location_id
            data["launch_type_id"] = launch_type_id
            data["launch_dt"] = datetime.strptime(data["launch_dt"], "%Y-%m-%dT%H:%M:%S")
            flight = Flight(**data)
            flight.save()
            flights[flight.flight_id] = flight

    return UnitTestDataHelper(flights)

def create_flight_by_request(app_client, data_list, pilot_capacities, gliders, locations, launch_types):

    responses = []
    flight_ids = []

    zipped_data = zip(data_list, pilot_capacities.ids, gliders.ids, locations.ids, launch_types.ids)
    for data, pilot_capacity_id, glider_id, location_id, launch_type_id in zipped_data:
        data = data.copy()

        data["pilot_capacity_id"] = pilot_capacity_id
        data["glider_id"] = glider_id
        data["location_id"] = location_id
        data["launch_type_id"] = launch_type_id

        data_json = json.dumps(data)
        response = JSONResponseHelper(app_client().post(ENDPOINT, data=data_json, headers={'content-type': 'application/json'}))
        responses.append(response)
        flight_ids.append(response.json["flight_id"])

    return flight_ids, responses


def add_iso_time(iso_time_str, delta_in_minutes, as_string=False):
    iso_time = datetime.strptime(iso_time_str, "%Y-%m-%dT%H:%M:%S")
    new_time = iso_time + timedelta(seconds = 60 * delta_in_minutes)

    if as_string:
        new_time = "{:%Y-%m-%dT%H:%M:%S}".format(new_time)

    return new_time

class TestFlight:

    def test_flight_creation_valid_data(self, app_client, pilot_capacities, gliders, locations, launch_types):

        ### Action ###

        _, responses = create_flight_by_request(app_client, FLIGHTS, pilot_capacities, gliders, locations, launch_types)

        ### Tests ###

        zipped_data = zip(responses, FLIGHTS, gliders.data, pilot_capacities.data, locations.data, launch_types.data)
        for resp, test_flight_data, glider, pilot_cap, location, launch_type in zipped_data:
            assert resp.status_code == 201
            
            resp.json.pop("flight_id")
            
            expected_data = test_flight_data.copy()
            expected_data["glider"] = glider.identifier()
            expected_data["glider_id"] = glider.glider_id
            expected_data["landing_dt"] = add_iso_time(expected_data["launch_dt"], expected_data["duration"], as_string=True)
            expected_data["launch_type"] = launch_type.description
            expected_data["launch_type_id"] = launch_type.launch_type_id
            expected_data["location"] = location.name
            expected_data["location_id"] = location.location_id
            expected_data["pilot_capacity"] = pilot_cap.text
            expected_data["pilot_capacity_id"] = pilot_cap.pilot_capacity_id
            expected_data["instructor"] = expected_data.get("instructor", '')
            expected_data["km_flown"] = expected_data.get("km_flown", 0)
            expected_data["launch_height"] = expected_data.get("launch_height", 0)
            expected_data["max_height"] = expected_data.get("max_height", 0)
            expected_data["notes"] = []

            assert expected_data == resp.json

    def test_flight_creation_missing_data_all_fields(self, app_client):

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=json.dumps({}), headers={'content-type': 'application/json'}))

        ### Tests ###

        assert resp.status_code == 400
        for key in ['launch_dt', 'duration', 'pilot_capacity_id', 'glider_id', 'location_id', 'launch_type_id']:
            assert key in resp.json["errors"]

    @pytest.mark.parametrize("missing_field", ['launch_dt', 'duration', 'pilot_capacity_id', 'glider_id', 'location_id', 'launch_type_id'])
    def test_flight_creation_missing_data(self, app_client, missing_field):

        ### Setup ###

        flight_data = create_flight_data(FLIGHTS[0], 1, 1, 1, 1)
        flight_data.pop(missing_field)

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=json.dumps(flight_data), headers={'content-type': 'application/json'}))
        
        ### Tests ###

        assert resp.status_code == 400
        assert missing_field in resp.json["errors"]

    @pytest.mark.parametrize("present_field,value",
        [
            ('duration', 10),
            ('pilot_capacity_id', 1),
            ('glider_id', 1),
            ('location_id', 1),
            ('launch_type_id', 1)
        ]
    )
    def test_flight_creation_missing_data_all_but_one_field(self, app_client, present_field, value):

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=json.dumps({present_field:value}), headers={'content-type': 'application/json'}))

        ### Tests ###

        expected_missing_fields = ['launch_dt', 'duration', 'pilot_capacity_id', 'glider_id', 'location_id', 'launch_type_id']
        expected_missing_fields.remove(present_field)

        assert resp.status_code == 400
        for key in expected_missing_fields:
            assert key in resp.json["errors"]

    @pytest.mark.parametrize("field, value",
        [
            ("launch_dt", 0),
            ("duration", "10"),
            ("launch_height", "1500"),
            ("max_height", "1500"),
            ("km_flown", "0"),
            ("instructor", 123)
        ]
    )
    def test_flight_creation_invalid_field_datatype(self, app_client, field, value, pilot_capacities, gliders, locations, launch_types):

        ### Setup ###

        pilot_capacity_id, glider_id, location_id, launch_type_id = get_foreign_keys(0, pilot_capacities, gliders, locations, launch_types)
        flight_data = create_flight_data(FLIGHTS[0], pilot_capacity_id, glider_id, location_id, launch_type_id)
        flight_data[field] = value

        ### Action ###

        resp = app_client().post(ENDPOINT, data=json.dumps(flight_data), headers={'content-type': 'application/json'})
        json_data = json.loads(resp.data)
        
        ### Tests ###

        assert resp.status_code == 400
        assert field in json_data["errors"]
        assert str(value) in json_data["errors"][field]

    def test_api_can_get_all_flights(self, app_client, pilot_capacities,  gliders, locations, launch_types):

        ### Setup ###

        flights = create_test_flights(FLIGHTS, pilot_capacities, gliders, locations, launch_types)

        ### Action ###
        
        resp = app_client().get(ENDPOINT)
        json_resps = resp.get_json()

        ### Tests ###

        assert resp.status_code == 200

        zipped_data = zip(json_resps, flights.ids, FLIGHTS, pilot_capacities.data, gliders.data, locations.data, launch_types.data)
        for json_resp, flight_id, flight, pilot_capacity, glider, location, launch_type in zipped_data:
            assert json_resp["flight_id"] == flight_id
            assert json_resp["launch_dt"] == flight["launch_dt"]
            assert json_resp["landing_dt"] == add_iso_time(flight["launch_dt"], flight["duration"], as_string=True)
            assert json_resp["duration"] == flight["duration"]
            assert json_resp["pilot_capacity_id"] == pilot_capacity.pilot_capacity_id
            assert json_resp["pilot_capacity"] == pilot_capacity.text
            assert json_resp["glider_id"] == glider.glider_id
            assert json_resp["glider"] == glider.identifier()
            assert json_resp["location_id"] == location.location_id
            assert json_resp["location"] == location.name
            assert json_resp["launch_type_id"] == launch_type.launch_type_id
            assert json_resp["launch_type"] == launch_type.description
            assert json_resp["launch_height"] == flight.get("launch_height", 0)
            assert json_resp["max_height"] == flight.get("launch_height", 0)
            assert json_resp["km_flown"] == flight.get("km_flown", 0)
            assert json_resp["instructor"] == flight.get("instructor", '')

    def test_api_can_get_flight_by_id(self, app_client, pilot_capacities,  gliders, locations, launch_types):

        ### Setup ###

        flights = create_test_flights(FLIGHTS, pilot_capacities, gliders, locations, launch_types)

        ### Action ###

        resps = []

        for flight_id in flights.ids: 
            resps.append(JSONResponseHelper(app_client().get(ENDPOINT + str(flight_id))))

        ### Tests ###

        zipped_data = zip(resps, flights.ids, FLIGHTS, pilot_capacities.data, gliders.data, locations.data, launch_types.data)
        for resp, flight_id, flight, pilot_capacity, glider, location, launch_type in zipped_data:
            assert resp.json["flight_id"] == flight_id
            assert resp.json["launch_dt"] == flight["launch_dt"]
            assert resp.json["landing_dt"] == add_iso_time(flight["launch_dt"], flight["duration"], as_string=True)
            assert resp.json["duration"] == flight["duration"]
            assert resp.json["pilot_capacity_id"] == pilot_capacity.pilot_capacity_id
            assert resp.json["pilot_capacity"] == pilot_capacity.text
            assert resp.json["glider_id"] == glider.glider_id
            assert resp.json["glider"] == glider.identifier()
            assert resp.json["location_id"] == location.location_id
            assert resp.json["location"] == location.name
            assert resp.json["launch_type_id"] == launch_type.launch_type_id
            assert resp.json["launch_type"] == launch_type.description
            assert resp.json["launch_height"] == flight.get("launch_height", 0)
            assert resp.json["max_height"] == flight.get("launch_height", 0)
            assert resp.json["km_flown"] == flight.get("km_flown", 0)
            assert resp.json["instructor"] == flight.get("instructor", '')

    @pytest.mark.parametrize("field, new_value",
        [
            ("duration", 60),
            ("launch_height", 700),
            ("max_height", 700),
            ("km_flown", 11),
            ("instructor", "Jane Doe"),
            ("launch_dt", "2020-08-02T15:15:09"),
        ]
    )
    def test_flight_can_be_edited_with_valid_data(self, app_client, field, new_value, pilot_capacities,  gliders, locations, launch_types):

        ### Setup ###

        flights = create_test_flights(FLIGHTS, pilot_capacities, gliders, locations, launch_types)
        flight_id = flights.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(flight_id), data=json.dumps({field: new_value}),
            headers={'content-type': 'application/json'}
        )

        ### Test ###

        assert resp.status_code == 200

        resp = app_client().get(ENDPOINT + str(flight_id))
        json_data = json.loads(resp.data)
            
        assert json_data[field] == new_value

    @pytest.mark.parametrize("field, invalid_datatype_value",
        [
            ("launch_dt", 0),
            ("duration", "60"),
            ("launch_height", "700"),
            ("max_height", "700"),
            ("km_flown", "11"),
            ("instructor", 123)
        ]
    )
    def test_flight_cannot_be_edited_with_invalid_datatype(self, app_client, field, invalid_datatype_value, pilot_capacities,  gliders, locations, launch_types):
        
        ### Setup ###

        flights = create_test_flights(FLIGHTS, pilot_capacities, gliders, locations, launch_types)
        flight_id = flights.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(flight_id), data=json.dumps({field: invalid_datatype_value}),
            headers={'content-type': 'application/json'}
        )

        ### Test ###

        assert resp.status_code == 400
        json_data = json.loads(resp.data)
        assert field in json_data["errors"]
        assert str(invalid_datatype_value) in json_data["errors"][field]

        resp = app_client().get(ENDPOINT + str(flight_id))
        json_data = json.loads(resp.data)

        assert json_data["launch_dt"] == "2020-07-31T12:00:00"
        assert json_data["duration"] == 10
        assert json_data["launch_height"] == 1500
        assert json_data["max_height"] == 1500
        assert json_data["km_flown"] == 0
        assert json_data["instructor"] == "John Doe"

    def test_flight_deletion(self, app_client, pilot_capacities,  gliders, locations, launch_types):

        ### Setup ###

        flights = create_test_flights(FLIGHTS, pilot_capacities, gliders, locations, launch_types)

        ### Action ###

        resp = app_client().delete(ENDPOINT + str(flights.ids[0]))
        assert resp.status_code == 200

        result = app_client().get(ENDPOINT + str(flights.ids[0]))
        assert result.status_code == 404

        result = app_client().get(ENDPOINT + str(flights.ids[1]))
        assert result.status_code == 200

    def test_flight_deletion_also_deletes_notes(self, app_client, pilot_capacities,  gliders, locations, launch_types):

        ### Setup ###

        flights = create_test_flights(FLIGHTS, pilot_capacities, gliders, locations, launch_types)
        flight_notes = create_test_flight_notes(FLIGHT_NOTES, flights)

        flight_id_to_delete = flights.ids[0]

        ### Action ###

        resp = app_client().delete(ENDPOINT + str(flights.ids[0]))

        ### Tests ###

        all_notes = JSONResponseHelper(app_client().get('/api/flight_note/'))
        for note in all_notes.json:
            assert note["flight_id"] != flight_id_to_delete

    def test_flight_deletion_also_deletes_files(self, app_client, pilot_capacities,  gliders, locations, launch_types):

        from tests.test_filetype import create_test_filetypes, FILETYPES

        ### Setup ###

        flights = create_test_flights(FLIGHTS, pilot_capacities, gliders, locations, launch_types)
        filetypes = create_test_filetypes(FILETYPES)
        flight_files = create_test_flight_files(FLIGHT_FILES, flights, filetypes)

        flight_id_to_delete = flights.ids[0]

        ### Action ###

        resp = app_client().delete(ENDPOINT + str(flights.ids[0]))

        ### Tests ###

        resp = JSONResponseHelper(app_client().get('/api/flight_file/'))
        for files in resp.json:
            assert files["flight_id"] != flight_id_to_delete
