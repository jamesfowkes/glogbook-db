import pytest
import os
import json

from collections import OrderedDict

from tests import JSONResponseHelper, UnitTestDataHelper, create_json_data
from tests.fixtures import app, app_client, pilot_capacities,  gliders, locations, launch_types

from tests.test_flight_file import create_test_flight_files, FLIGHT_FILES
from tests.test_flight import create_test_flights, FLIGHTS

from app.models.enumerations import Filetype

FILETYPES = [
    {"description": "Video"},
    {"description": "GPS Trace"}
]

ENDPOINT = '/api/filetype/'

def create_test_filetypes(data_list):

    with app.app_context():
        filetypes = {}
        for data in data_list:
            filetype = Filetype(**data)
            filetype.save()
            filetypes[filetype["filetype_id"]] = filetype

    return UnitTestDataHelper(filetypes)

def create_filetypes_by_request(client, data_list):

    responses = []
    filetype_ids = []
    for data in data_list:
        data_json = json.dumps(data)
        response = client().post(ENDPOINT, data=data_json, headers={'content-type': 'application/json'}) 
        response = JSONResponseHelper(response)
        responses.append(response)
        filetype_ids.append(response.json["filetype_id"])

    return filetype_ids, responses

class TestFiletype:

    def test_filetype_creation_valid_data(self, app_client):

        ### Action ###

        _, responses = create_filetypes_by_request(app_client, FILETYPES)

        ### Tests ###
        
        for resp, expected_data in zip(responses, FILETYPES):
            assert resp.status_code == 201
            assert resp.json["filetype_id"] is not None
            assert resp.json["description"] == expected_data["description"]

    def test_filetype_creation_missing_data_all_fields(self, app_client):

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=json.dumps({}), headers={'content-type': 'application/json'}))
        
        ### Tests ###

        assert resp.status_code == 400
        assert "description" in resp.json["errors"]

    def test_filetype_creation_missing_description(self, app_client):

        ### Setup ###

        invalid_data = create_json_data(FILETYPES[0], missing_fields=["description"])

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'}))

        ### Tests ###

        assert resp.status_code == 400
        assert "description" in resp.json["errors"]

    def test_filetype_creation_invalid_description(self, app_client):

        ### Setup ###

        invalid_data = create_json_data(FILETYPES[0], invalid_fields={"description":1})

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'}))

        assert resp.status_code == 400
        assert "description" in resp.json["errors"]
        assert "1" in resp.json["errors"]["description"]

    def test_api_can_get_all_filetypes(self, app_client):
        
        ### Setup ###

        filetypes = create_test_filetypes(FILETYPES)

        ### Action ###

        resp = JSONResponseHelper(app_client().get(ENDPOINT))

        ### Tests ###

        assert resp.status_code == 200

        for i in range(len(FILETYPES)):
            resp.json[i].pop("filetype_id")
            assert resp.json[i] == FILETYPES[i]

    def test_api_can_get_filetype_by_id(self, app_client):

        ### Setup ###

        filetypes = create_test_filetypes(FILETYPES)

        ### Action ###

        responses = []
        for filetype_id in filetypes.ids:
            resp = JSONResponseHelper(app_client().get(ENDPOINT + str(filetype_id)))
            responses.append(resp)

        ### Tests ###

        for response, expected_filetype_id, expected_filetype in zip(responses, filetypes.ids, FILETYPES): 
            assert response.status_code == 200
            assert response.json["filetype_id"] == expected_filetype_id
            assert response.json["description"] == expected_filetype["description"]

    def test_filetype_can_be_edited_with_valid_data(self, app_client):

        ### Setup ###

        filetypes = create_test_filetypes(FILETYPES)
        filetype_id = filetypes.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(filetype_id), data=json.dumps({"description": "Barograph Trace"}),
            headers={'content-type': 'application/json'}
        )

        ### Tests ###

        assert resp.status_code == 200

        resp = JSONResponseHelper(app_client().get(ENDPOINT + str(filetype_id)))
            
        assert resp.json["description"] == "Barograph Trace"

    def test_filetype_cannot_be_edited_with_invalid_str_data(self, app_client):

        ### Setup ###

        filetypes = create_test_filetypes(FILETYPES)
        filetype_id = filetypes.ids[0]
        
        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(filetype_id), data=json.dumps({"description": 1}),
            headers={'content-type': 'application/json'}
        )

        ### Tests ###

        assert resp.status_code == 400

        assert "description" in resp.json["errors"]
        assert "1" in resp.json["errors"]["description"]

        resp = JSONResponseHelper(app_client().get(ENDPOINT + str(filetype_id)))
        assert resp.json["description"] == FILETYPES[0]["description"]

    def test_filetype_deletion(self, app_client):

        ### Setup ###

        filetypes = create_test_filetypes(FILETYPES)

        ### Action ###

        resp = app_client().delete(ENDPOINT + str(filetypes.ids[0]))

        ### Tests ###

        assert resp.status_code == 200

        result = app_client().get(ENDPOINT + str(filetypes.ids[0]))
        assert result.status_code == 404

        result = app_client().get(ENDPOINT + str(filetypes.ids[1]))
        assert result.status_code == 200

    def test_filetype_cannot_be_deleted_with_associated_file(self, app_client, pilot_capacities, gliders, locations, launch_types):

        ### Setup ###

        flights = create_test_flights(FLIGHTS, pilot_capacities, gliders, locations, launch_types)
        filetypes = create_test_filetypes(FILETYPES)
        flight_files = create_test_flight_files(FLIGHT_FILES, flights, filetypes)

        flight_files_id_to_delete = flight_files.ids[0]

        ### Action ###

        resp = app_client().delete(ENDPOINT + str(flight_files_id_to_delete))

        ### Tests ###

        assert resp.status_code == 400

        result = app_client().get(ENDPOINT + str(flight_files_id_to_delete))
        assert result.status_code == 200
