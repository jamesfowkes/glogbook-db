import pytest
import os
import json

from collections import OrderedDict

from tests import JSONResponseHelper, UnitTestDataHelper, create_json_data
from tests.fixtures import app,app_client, flights

from app.models.enumerations import LaunchType

LAUNCH_TYPES = [
    {"launch_type_id": "W", "description": "Winch Launch"},
    {"launch_type_id": "A", "description": "Aerotow"},
    {"launch_type_id": "B", "description": "Bungee"},
    {"launch_type_id": "V", "description": "Autotow"},
    {"launch_type_id": "S", "description": "Self-launch"},
]

ENDPOINT = '/api/launch_type/'

def create_test_launch_types(data_list):

    with app.app_context():
        launch_types = {}
        for data in data_list:
            launch_type = LaunchType(**data)
            launch_type.save()
            launch_types[launch_type["launch_type_id"]] = launch_type

    return UnitTestDataHelper(launch_types)

def create_launch_types_by_request(client, data_list):

    responses = []
    launch_type_ids = []
    for data in data_list:
        data_json = json.dumps(data)
        response = client().post(ENDPOINT, data=data_json, headers={'content-type': 'application/json'}) 
        response = JSONResponseHelper(response)
        responses.append(response)
        launch_type_ids.append(response.json["launch_type_id"])

    return launch_type_ids, responses

class TestLaunchType:

    def test_launch_type_creation_valid_data(self, app_client):

        ### Action ###

        _, responses = create_launch_types_by_request(app_client, LAUNCH_TYPES)

        ### Tests ###
        
        for resp, expected_data in zip(responses, LAUNCH_TYPES):
            assert resp.status_code == 201
            assert resp.json == expected_data

    def test_launch_type_creation_missing_data_all_fields(self, app_client):

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=json.dumps({}), headers={'content-type': 'application/json'}))
        
        ### Tests ###

        assert resp.status_code == 400
        assert "description" in resp.json["errors"]

    def test_launch_type_creation_missing_description(self, app_client):

        ### Setup ###

        invalid_data = create_json_data(LAUNCH_TYPES[0], missing_fields=["description"])

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'}))

        ### Tests ###

        assert resp.status_code == 400
        assert "description" in resp.json["errors"]

    def test_launch_type_creation_invalid_description(self, app_client):

        ### Setup ###

        invalid_data = create_json_data(LAUNCH_TYPES[0], invalid_fields={"description":1})

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'}))

        assert resp.status_code == 400
        assert "description" in resp.json["errors"]
        assert "1" in resp.json["errors"]["description"]

    def test_api_can_get_all_launch_types(self, app_client):
        
        ### Setup ###

        _ = create_test_launch_types(LAUNCH_TYPES)

        ### Action ###

        resp = JSONResponseHelper(app_client().get(ENDPOINT))

        ### Tests ###

        assert resp.status_code == 200

        for i in range(len(LAUNCH_TYPES)):
            assert resp.json[i] == LAUNCH_TYPES[i]

    def test_api_can_get_launch_type_by_id(self, app_client):

        ### Setup ###

        launch_types = create_test_launch_types(LAUNCH_TYPES)

        ### Action ###

        responses = []
        for launch_type_id in launch_types.ids:
            resp = JSONResponseHelper(app_client().get(ENDPOINT + str(launch_type_id)))
            responses.append(resp)

        ### Tests ###

        for response, expected_launch_type_id, expected_launch_type in zip(responses, launch_types.ids, LAUNCH_TYPES): 
            assert response.status_code == 200
            assert response.json["launch_type_id"] == expected_launch_type_id
            assert response.json["description"] == expected_launch_type["description"]

    def test_launch_type_can_be_edited_with_valid_data(self, app_client):

        ### Setup ###

        launch_types = create_test_launch_types(LAUNCH_TYPES)
        launch_type_id = launch_types.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(launch_type_id), data=json.dumps({"description": "Winch Launch Edited"}),
            headers={'content-type': 'application/json'}
        )

        ### Tests ###

        assert resp.status_code == 200

        resp = JSONResponseHelper(app_client().get(ENDPOINT + str(launch_type_id)))
            
        assert resp.json["description"] == "Winch Launch Edited"

    def test_launch_type_cannot_be_edited_with_invalid_str_data(self, app_client):

        ### Setup ###

        launch_types = create_test_launch_types(LAUNCH_TYPES)
        launch_type_id = launch_types.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(launch_type_id), data=json.dumps({"description": 1}),
            headers={'content-type': 'application/json'}
        )

        ### Tests ###

        assert resp.status_code == 400

        assert "description" in resp.json["errors"]
        assert "1" in resp.json["errors"]["description"]

        resp = JSONResponseHelper(app_client().get(ENDPOINT + str(launch_type_id)))
        assert resp.json["description"] == LAUNCH_TYPES[0]["description"]

    def test_launch_type_deletion(self, app_client):

        ### Setup ###

        launch_types = create_test_launch_types(LAUNCH_TYPES)
        launch_type_id = launch_types.ids[0]

        ### Action ###

        resp = app_client().delete(ENDPOINT + str(launch_types.ids[0]))

        ### Tests ###

        assert resp.status_code == 200

        result = app_client().get(ENDPOINT + str(launch_types.ids[0]))
        assert result.status_code == 404

        result = app_client().get(ENDPOINT + str(launch_types.ids[1]))
        assert result.status_code == 200

    def test_launch_type_cannot_be_deleted_with_associated_flight(self, app_client, flights):

        ### Action ###

        launch_type_id = flights.data[0].launch_type_id
        resp = app_client().delete(ENDPOINT + str(launch_type_id))

        ### Tests ###

        assert resp.status_code == 400

        result = app_client().get(ENDPOINT + str(launch_type_id))
        assert result.status_code == 200
