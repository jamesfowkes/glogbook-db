import json

class JSONResponseHelper:

    def __init__(self, response):
        self.response = response
        self.status_code = response.status_code
        self.json = response.get_json()

class UnitTestDataHelper:

    def __init__(self, test_data):
        self.ids = list(test_data.keys())
        self.data = list(test_data.values())
        self.map = test_data

def create_json_data(input_data, missing_fields=None, invalid_fields=None, add_fields=None):

    missing_fields = missing_fields or []
    invalid_fields = invalid_fields or {}
    add_fields = add_fields or {}
    
    data_copy = input_data.copy()

    for missing_field in missing_fields:
        try:
            data_copy.pop(missing_field)
        except KeyError:
            pass

    for fieldname, invalid_datum in invalid_fields.items():
        data_copy[fieldname] = invalid_datum

    for fieldname, new_datum in add_fields.items():
        data_copy[fieldname] = new_datum

    return json.dumps(data_copy)
