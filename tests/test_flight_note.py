import pytest
import os
import json

from collections import OrderedDict

from tests.fixtures import app, app_client, flights
from tests import JSONResponseHelper, UnitTestDataHelper

from app.models.flight_note import FlightNote

FLIGHT_NOTES = [
    {"text": "Currency Check"},
    {"text": "Second part of 100k diploma"}
]

ENDPOINT = '/api/flight_note/'

def create_test_flight_notes(data_list, flights):

    with app.app_context():
        flight_notes = {}
        for data, flight_id in zip(data_list, flights.ids):
            data = data.copy()
            data["flight_id"] = flight_id
            flight_note = FlightNote(**data)
            flight_note.save()
            flight_notes[flight_note.flight_note_id] = flight_note

    return UnitTestDataHelper(flight_notes)

def create_flight_notes_by_request(client, data_list, flights):

    responses = []
    flight_note_ids = []
    for data, flight_id in zip(data_list, flights.ids):
        data_copy = data.copy()
        data_copy["flight_id"] = flight_id
        data_json = json.dumps(data_copy)
        response = client().post(ENDPOINT, data=data_json, headers={'content-type': 'application/json'}) 
        response = JSONResponseHelper(response)
        flight_note_ids.append(response.json["flight_note_id"])

    return flight_note_ids, responses

def get_note_json_for_id(client, flight_note_id):
    res = client().get(ENDPOINT + str(flight_note_id))
    return json.loads(res.data)

def create_flight_note_data(data, flight_id, text):
    data_copy = data.copy()
    data_copy["flight_id"] = flight_id
    data_copy["text"] = text
    
    return data_copy
     
class TestFlightNote:
   
    def test_flight_note_creation_valid_data(self, app_client, flights):

        ### Action ### 

        _, responses = create_flight_notes_by_request(app_client, FLIGHT_NOTES, flights)

        ### Tests ###

        for response, flight_id, note_data in zip(responses, flights.ids, FLIGHT_NOTES):
            
            assert response.status_code == 201
                
            assert response.json["flight_note_id"] is not None
            assert response.json["flight_id"] == flight_id
            assert response.json["text"] == note_data["text"]

    def test_flight_note_creation_missing_data_all_fields(self, app_client, flights):

        ### Action ###

        res = app_client().post(ENDPOINT, data=json.dumps({}), headers={'content-type': 'application/json'})
        json_data = json.loads(res.data)

        ### Tests ###
        
        assert res.status_code == 400
        assert "flight_id" in json_data["errors"]
        assert "text" in json_data["errors"]
        assert len(json_data["errors"]) == 2

    def test_flight_note_creation_missing_flight_id(self, app_client, flights):

        ### Action ###

        res = app_client().post(ENDPOINT, data=json.dumps(FLIGHT_NOTES[0]), headers={'content-type': 'application/json'})
        json_data = json.loads(res.data)

        ### Tests ###
        
        assert res.status_code == 400
        assert "flight_id" in json_data["errors"]

    def test_flight_note_creation_missing_text(self, app_client, flights):

        ### Action ###

        res = app_client().post(ENDPOINT, data=json.dumps({"flight_id": 1}), headers={'content-type': 'application/json'})
        json_data = json.loads(res.data)
        
        ### Tests ###

        assert res.status_code == 400
        assert "text" in json_data["errors"]

    @pytest.mark.parametrize("field, value",
        [
            ('flight_id', "1"),
            ('text', 1),
        ]
    )
    def test_flight_note_creation_invalid_data(self, app_client, field, value):

        ### Setup ###

        invalid_data = create_flight_note_data(FLIGHT_NOTES[0], 1, "1")
        invalid_data[field] = value

        ### Action ###

        res = app_client().post(ENDPOINT, data=json.dumps(invalid_data), headers={'content-type': 'application/json'})
        json_data = json.loads(res.data)
        
        ### Tests ###

        assert res.status_code == 400
        assert field in json_data["errors"]
        assert str(value) in json_data["errors"][field]

    def test_api_can_get_all_flight_note(self, app_client, flights):

        ### Setup ###
        
        flight_notes = create_test_flight_notes(FLIGHT_NOTES, flights)

        ### Action ###
        
        res = app_client().get(ENDPOINT)
        json_resps = res.get_json()

        ### Tests ###

        assert res.status_code == 200

        for json_resp, flight_note_id, flight_note, flight_id in zip(json_resps, flight_notes.ids, FLIGHT_NOTES, flights.ids):
            assert json_resp["flight_note_id"] == flight_note_id
            assert json_resp["flight_id"] == flight_id
            assert json_resp["text"] == flight_note["text"]

    def test_api_can_get_flight_note_by_id(self, app_client, flights):

        ### Setup ###

        flight_notes = create_test_flight_notes(FLIGHT_NOTES, flights)

        ### Action ###

        resps = []

        for flight_note_id in flight_notes.ids: 
            resps.append(JSONResponseHelper(app_client().get(ENDPOINT + str(flight_note_id))))

        ### Tests ###

        for resp, flight_note_id, flight_note_data, flight_id in zip(resps, flight_notes.ids, FLIGHT_NOTES, flights.ids): 
            assert resp.status_code == 200
            assert resp.json["flight_note_id"] == flight_note_id
            assert resp.json["flight_id"] == flight_id
            assert resp.json["text"] == flight_note_data["text"]

    def test_flight_note_can_be_edited_with_valid_data(self, app_client, flights):
        
        ### Setup ###

        flight_notes = create_test_flight_notes(FLIGHT_NOTES, flights)
        flight_note_id = flight_notes.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(flight_note_id), data=json.dumps({"text": "Landed out!"}), headers={'content-type': 'application/json'}
        )
        
        json_data = get_note_json_for_id(app_client, flight_note_id)

        ### Tests ###

        assert resp.status_code == 200
        assert json_data["flight_id"] == flights.ids[0]
        assert json_data["flight_note_id"] == flight_note_id
        assert json_data["text"] == "Landed out!"

    def test_flight_note_cannot_be_edited_with_invalid_data(self, app_client, flights):

        ### Setup ###
        
        flight_notes = create_test_flight_notes(FLIGHT_NOTES, flights)
        flight_note_id = flight_notes.ids[0]
        
        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(flight_note_id), data=json.dumps({"text": 1}), headers={'content-type': 'application/json'}
        )
        json_data = get_note_json_for_id(app_client, flight_note_id)

        ### Tests ###

        assert resp.status_code == 400            
        assert json_data["flight_id"] == flights.ids[0]
        assert json_data["flight_note_id"] == flight_note_id
        assert json_data["text"] == FLIGHT_NOTES[0]["text"]

    def test_flight_note_deletion(self, app_client, flights):

        ### Setup ###

        flight_notes = create_test_flight_notes(FLIGHT_NOTES, flights)

        ### Action ###

        resp = app_client().delete(ENDPOINT + str(flight_notes.ids[0]))
        
        ### Tests ###

        assert resp.status_code == 200

        result = app_client().get(ENDPOINT + str(flight_notes.ids[0]))
        assert result.status_code == 404

        result = app_client().get(ENDPOINT + str(flight_notes.ids[1]))
        assert result.status_code == 200
