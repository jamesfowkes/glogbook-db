import pytest
import os
import json

from collections import OrderedDict

from tests import JSONResponseHelper, UnitTestDataHelper, create_json_data
from tests.fixtures import app, app_client, flights

from app.models.enumerations import PilotCapacity

PILOT_CAPACITIES = [
    {"text": "P1", "description": "Solo pilot"},
    {"text": "P1 Dual", "description": "P1 on non-instructional dual flight"},
    {"text": "P2 Dual", "description": "P2 on non-instructional dual flight"},
    {"text": "P2 Training", "description": "P2 on instructional dual flight"},
    {"text": "Instructor", "description": "P1 on instructional dual flight"}
]

ENDPOINT = '/api/pilot_capacity/'

def create_test_pilot_capacities(data_list):

    with app.app_context():
        pilot_capacities = {}
        for data in data_list:
            pilot_capacity = PilotCapacity(**data)
            pilot_capacity.save()
            pilot_capacities[pilot_capacity["pilot_capacity_id"]] = pilot_capacity

    return UnitTestDataHelper(pilot_capacities)

def create_pilot_capacities_by_request(client, data_list):

    responses = []
    pilot_capacities = []
    for data in data_list:
        data_json = json.dumps(data)
        response = client().post(ENDPOINT, data=data_json, headers={'content-type': 'application/json'}) 
        response = JSONResponseHelper(response)
        responses.append(response)
        pilot_capacities.append(response.json["pilot_capacity_id"])

    return pilot_capacities, responses

class TestLocation:

    def test_pilot_capacity_creation_valid_data(self, app_client):

        ### Action ###

        _, responses = create_pilot_capacities_by_request(app_client, PILOT_CAPACITIES)

        ### Tests ###
        
        for resp, expected_data in zip(responses, PILOT_CAPACITIES):
            assert resp.status_code == 201
            resp.json.pop("pilot_capacity_id")
            assert resp.json == expected_data

    def test_pilot_capacity_creation_missing_data_all_fields(self, app_client):

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=json.dumps({}), headers={'content-type': 'application/json'}))
        
        ### Tests ###

        assert resp.status_code == 400
        assert "text" in resp.json["errors"]
        assert "description" in resp.json["errors"]

    @pytest.mark.parametrize("missing_field", ['text', 'description'])
    def test_pilot_capacity_creation_missing_data_one_field(self, app_client, missing_field):

        ### Setup ###

        invalid_data = create_json_data(PILOT_CAPACITIES[0], missing_fields=[missing_field])

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'}))

        ### Tests ###

        assert resp.status_code == 400
        assert missing_field in resp.json["errors"]

    @pytest.mark.parametrize("field, value",
        [
            ("text", 0),
            ("description", 0)
        ]
    )
    def test_pilot_capacity_creation_invalid_str_data(self, app_client, field, value):

        ### Setup ###

        invalid_data = create_json_data(PILOT_CAPACITIES[0], invalid_fields={field:value})

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'}))

        assert resp.status_code == 400
        assert field in resp.json["errors"]
        assert str(value) in resp.json["errors"][field]

    def test_api_can_get_all_pilot_capacities(self, app_client):
        
        ### Setup ###

        pilot_capacities = create_test_pilot_capacities(PILOT_CAPACITIES)

        ### Action ###

        resp = JSONResponseHelper(app_client().get(ENDPOINT))

        ### Tests ###

        assert resp.status_code == 200

        for i in range(len(PILOT_CAPACITIES)):
            assert resp.json[i].pop("pilot_capacity_id") == pilot_capacities.ids[i]    
            assert resp.json[i] == PILOT_CAPACITIES[i]

    def test_api_can_get_pilot_capacity_by_id(self, app_client):

        ### Setup ###

        pilot_capacities = create_test_pilot_capacities(PILOT_CAPACITIES)

        ### Action ###

        responses = []
        for pilot_capacity_id in pilot_capacities.ids:
            resp = JSONResponseHelper(app_client().get(ENDPOINT + str(pilot_capacity_id)))
            responses.append(resp)

        ### Tests ###

        for response, expected_pilot_capacity_id, expected_pilot_capacity in zip(responses, pilot_capacities.ids, PILOT_CAPACITIES): 
            assert response.status_code == 200
            assert response.json["pilot_capacity_id"] == expected_pilot_capacity_id
            assert response.json["text"] == expected_pilot_capacity["text"]
            assert response.json["description"] == expected_pilot_capacity["description"]

    def test_api_can_get_pilot_capacity_by_id_returns_404_for_invalid_id(self, app_client):

        ### Setup ###

        _ = create_test_pilot_capacities(PILOT_CAPACITIES)

        ### Action ###

        response = JSONResponseHelper(app_client().get(ENDPOINT + "999"))

        ### Tests ###

        assert response.status_code == 404

    def test_pilot_capacity_can_be_edited_with_valid_data(self, app_client):

        ### Setup ###

        pilot_capacities = create_test_pilot_capacities(PILOT_CAPACITIES)
        pilot_capacity_id = pilot_capacities.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(pilot_capacity_id), data=json.dumps({"description": "Pilot in Command (Solo)"}),
            headers={'content-type': 'application/json'}
        )

        ### Tests ###

        assert resp.status_code == 200

        resp = JSONResponseHelper(app_client().get(ENDPOINT + str(pilot_capacity_id)))
            
        assert resp.json["text"] == "P1"
        assert resp.json["description"] == "Pilot in Command (Solo)"

    def test_pilot_capacity_cannot_edit_text(self, app_client):

        ### Setup ###

        pilot_capacities = create_test_pilot_capacities(PILOT_CAPACITIES)
        pilot_capacity_id = pilot_capacities.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(pilot_capacity_id), data=json.dumps({"text": "PIC", "description": "Pilot in Command (Solo)"}),
            headers={'content-type': 'application/json'}
        )

        ### Tests ###

        assert resp.status_code == 200

        resp = JSONResponseHelper(app_client().get(ENDPOINT + str(pilot_capacity_id)))
            
        assert resp.json["text"] == "P1"
        assert resp.json["description"] == "Pilot in Command (Solo)"

    def test_pilot_capacity_cannot_be_edited_with_invalid_str_data(self, app_client):

        ### Setup ###

        pilot_capacities = create_test_pilot_capacities(PILOT_CAPACITIES)
        pilot_capacity_id = pilot_capacities.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(pilot_capacity_id), data=json.dumps({"description": 1}),
            headers={'content-type': 'application/json'}
        )

        ### Tests ###

        assert resp.status_code == 400

        assert "description" in resp.json["errors"]
        assert "1" in resp.json["errors"]["description"]

        resp = JSONResponseHelper(app_client().get(ENDPOINT + str(pilot_capacity_id)))
        assert resp.json["description"] == PILOT_CAPACITIES[0]["description"]

    def test_pilot_capacity_deletion(self, app_client):

        ### Setup ###

        pilot_capacities = create_test_pilot_capacities(PILOT_CAPACITIES)

        ### Action ###

        resp = app_client().delete(ENDPOINT + str(pilot_capacities.ids[0]))

        ### Tests ###

        assert resp.status_code == 200

        result = app_client().get(ENDPOINT + str(pilot_capacities.ids[0]))
        assert result.status_code == 404

        result = app_client().get(ENDPOINT + str(pilot_capacities.ids[1]))
        assert result.status_code == 200

    def test_pilot_capacity_cannot_be_deleted_with_associated_flight(self, app_client, flights):

        ### Action ###

        pilot_capacity_id = flights.data[0].pilot_capacity_id
        resp = app_client().delete(ENDPOINT + str(pilot_capacity_id))

        ### Tests ###

        assert resp.status_code == 400

        result = app_client().get(ENDPOINT + str(pilot_capacity_id))
        assert result.status_code == 200
