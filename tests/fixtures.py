import json
import pytest

from app import create_app
from app.database import db

app = create_app(None)

@pytest.fixture
def pilot_capacities():

    from tests.test_pilot_capacity import PILOT_CAPACITIES, create_test_pilot_capacities
    return create_test_pilot_capacities(PILOT_CAPACITIES)

@pytest.fixture
def launch_types():
    from tests.test_launch_type import LAUNCH_TYPES, create_test_launch_types
    return create_test_launch_types(LAUNCH_TYPES)

@pytest.fixture
def filetypes():
    from tests.test_filetype import FILETYPES, create_test_filetypes
    return create_test_filetypes(FILETYPES)

@pytest.fixture
def app_client():
    
    with app.app_context():
        db.drop_all()
        db.create_all()

    return app.test_client

@pytest.fixture
def glider_models():
    from tests.test_glider_model import GLIDER_MODELS, create_test_glider_models
    return create_test_glider_models(GLIDER_MODELS)
    
@pytest.fixture
def gliders():

    from tests.test_glider import GLIDERS, create_test_gliders
    from tests.test_glider_model import GLIDER_MODELS, create_test_glider_models

    glider_models = create_test_glider_models(GLIDER_MODELS)
    return create_test_gliders(GLIDERS, glider_models)

@pytest.fixture
def locations():
    from tests.test_location import LOCATIONS, create_test_locations
    return create_test_locations(LOCATIONS)

@pytest.fixture
def flights():

    from tests.test_glider import GLIDERS, create_test_gliders
    from tests.test_glider_model import GLIDER_MODELS, create_test_glider_models
    from tests.test_location import LOCATIONS, create_test_locations
    from tests.test_flight import FLIGHTS, create_test_flights
    from tests.test_pilot_capacity import PILOT_CAPACITIES, create_test_pilot_capacities
    from tests.test_launch_type import LAUNCH_TYPES, create_test_launch_types
    from tests.test_flight_file import FLIGHT_FILES, create_test_flight_files
    from tests.test_flight_note import FLIGHT_NOTES, create_test_flight_notes
    from tests.test_filetype import FILETYPES, create_test_filetypes

    glider_models = create_test_glider_models(GLIDER_MODELS)
    
    gliders = create_test_gliders(GLIDERS, glider_models)
    
    locations = create_test_locations(LOCATIONS)

    pilot_capacities = create_test_pilot_capacities(PILOT_CAPACITIES)
    
    launch_types = create_test_launch_types(LAUNCH_TYPES)
    
    flights = create_test_flights(FLIGHTS, pilot_capacities, gliders, locations, launch_types)
    
    filetypes = create_test_filetypes(FILETYPES)

    return flights

