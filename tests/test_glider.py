import pytest
import os
import json

from collections import OrderedDict

from tests import JSONResponseHelper, UnitTestDataHelper, create_json_data
from tests.fixtures import app, app_client, glider_models, flights

from app.models.glider import Glider

GLIDERS = [
    {"trigraph": "HPE"},
    {"trigraph": "ABC", "comp_no": "ZZZ", "greg": "GCABC"}
]

ENDPOINT = '/api/glider/'

def create_test_gliders(data_list, glider_models):

    with app.app_context():
        gliders = {}
        for data, glider_model in zip(data_list, glider_models.data):
            data = data.copy()

            data["glider_model_id"] = glider_model.glider_model_id
            if "comp_no" not in data:
                data["comp_no"] = ""
            if "greg" not in data:
                data["greg"] = ""
                            
            glider = Glider(**data)
            glider.save()
            gliders[glider.glider_id] = glider

    return UnitTestDataHelper(gliders)

def create_gliders_by_request(client, data_list, glider_models):

    responses = []
    glider_ids = []
    for data, glider_model_id in zip(data_list, glider_models.ids):
        data_copy = data.copy()
        data_copy["glider_model_id"] = glider_model_id
        data_json = json.dumps(data_copy)
        response = JSONResponseHelper(client().post(ENDPOINT, data=data_json, headers={'content-type': 'application/json'}))
        responses.append(response)
        glider_ids.append(response.json["glider_id"])

    return glider_ids, responses

class TestGlider:
    
    def test_glider_creation(self, app_client, glider_models):

        ### Action ###

        _, responses = create_gliders_by_request(app_client, GLIDERS, glider_models)

        ### Tests ###

        for response, glider_model, glider_data in zip(responses, glider_models.data, GLIDERS):
            assert response.status_code == 201
            
            assert response.json["glider_id"] is not None
            assert response.json["glider_model_id"] == glider_model.glider_model_id
            assert response.json["glider_model"] == glider_model.name
            assert response.json["trigraph"] == glider_data["trigraph"] 
            assert response.json["comp_no"] == glider_data.get("comp_no", "")
            assert response.json["greg"] == glider_data.get("greg", "")

    def test_glider_creation_missing_data_all_fields(self, app_client, glider_models):

        ### Action ###

        resp = app_client().post(ENDPOINT, data=json.dumps({}), headers={'content-type': 'application/json'})
        json_data = json.loads(resp.data)

        ### Tests ###
        
        assert resp.status_code == 400
        for key in ["glider_model_id", "trigraph"]:
            assert key in json_data["errors"]

    def test_glider_creation_missing_type_id_only(self, app_client, glider_models):

        ### Setup ###

        invalid_data = create_json_data(GLIDERS[0], missing_fields=["glider_model_id"])

        ### Action ###
        
        resp = app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'})

        ### Tests ###        
        
        assert resp.status_code == 400
        
    def test_glider_creation_missing_trigraph_only(self, app_client, glider_models):
        
        ### Setup ###

        invalid_data = create_json_data(GLIDERS[0], missing_fields=["trigraph"])

        ### Action ###
        
        resp = app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'})

        ### Tests ###
        
        assert resp.status_code == 400
        
    @pytest.mark.parametrize("field, value",
        [
            ('trigraph', 1),
            ('comp_no', 1),
            ('greg', 1),
            ("glider_model_id", "somestring"),
        ]
    )
    def test_glider_creation_invalid_data(self, app_client, field, value):

        ### Setup ###

        invalid_data = create_json_data(GLIDERS[0], invalid_fields={field: value})

        ### Action ###

        resp = app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'})
        json_data = json.loads(resp.data)

        ### Tests ###

        assert resp.status_code == 400
        assert field in json_data["errors"]
        assert str(value) in json_data["errors"][field]

    def test_api_can_get_all_gliders(self, app_client, glider_models):

        ### Setup ###

        gliders = create_test_gliders(GLIDERS, glider_models)
    
        ### Action ###

        resp = app_client().get(ENDPOINT)
        json_data = json.loads(resp.data)

        ### Tests ###

        assert resp.status_code == 200

        zipped_data = zip(json_data, gliders.ids, GLIDERS, glider_models.data)
        for json_datum, expected_glider_id, expected_glider_data, expected_glider_model in zipped_data: 
            assert json_datum["glider_id"] == expected_glider_id
            assert json_datum["glider_model_id"] == expected_glider_model.glider_model_id
            assert json_datum["glider_model"] == expected_glider_model.name
            assert json_datum["trigraph"] == expected_glider_data["trigraph"]
            assert json_datum["comp_no"] == expected_glider_data.get("comp_no", "")
            assert json_datum["greg"] == expected_glider_data.get("greg", "")
            
    def test_api_can_get_glider_by_id(self, app_client, glider_models):

        ### Setup ###

        gliders = create_test_gliders(GLIDERS, glider_models)

        ### Action ###

        responses = []
        for glider_id in gliders.ids:
            resp = JSONResponseHelper(app_client().get(ENDPOINT + str(glider_id)))
            responses.append(resp)

        ### Tests ###

        zipped_data = zip(responses, gliders.ids, GLIDERS, glider_models.data)
        for response, expected_glider_id, expected_glider_data, expected_glider_model in zipped_data:
            assert response.status_code == 200
            assert response.json["glider_id"] == expected_glider_id
            assert response.json["glider_model_id"] == expected_glider_model.glider_model_id
            assert response.json["glider_model"] == expected_glider_model.name
            assert response.json["trigraph"] == expected_glider_data["trigraph"]
            assert response.json["comp_no"] == expected_glider_data.get("comp_no", "")
            assert response.json["greg"] == expected_glider_data.get("greg", "")

    @pytest.mark.parametrize("field, value",
        [
            ('trigraph', 'ABC'),
            ('comp_no', 'NU'),
            ('greg', "GABCD")
        ]
    )
    def test_glider_can_be_edited_with_valid_data(self, app_client, glider_models, field, value):

        ### Setup ###

        gliders = create_test_gliders(GLIDERS, glider_models)
        glider_id = gliders.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(glider_id), data=json.dumps({field: value}),
            headers={'content-type': 'application/json'}
        )

        ### Tests ###

        assert resp.status_code == 200

        resp = JSONResponseHelper(app_client().get(ENDPOINT + str(glider_id)))

        assert resp.json[field] == value

    @pytest.mark.parametrize("field, value",
        [
            ('trigraph', 1),
            ('comp_no', 1),
            ('greg', 1),
            ('glider_model_id', 'notanint')
        ]
    )
    def test_glider_cannot_be_edited_with_invalid_data(self, app_client, glider_models, field, value):

        ### Setup ###

        gliders = create_test_gliders(GLIDERS, glider_models)
        glider_id = gliders.ids[0]

        ### Action ###

        resp = JSONResponseHelper(
            app_client().put(
            ENDPOINT + str(glider_id), data=json.dumps({field: value}),
            headers={'content-type': 'application/json'}
            )
        )

        ### Tests ###

        assert resp.status_code == 400
        assert field in resp.json["errors"]
        assert str(value) in resp.json["errors"][field]

    def test_glider_deletion(self, app_client, glider_models):

        ### Setup ###

        gliders = create_test_gliders(GLIDERS, glider_models)

        ### Action ###

        res = app_client().delete(ENDPOINT + str(gliders.ids[0]))
        assert res.status_code == 200

        result = app_client().get(ENDPOINT + str(gliders.ids[0]))
        assert result.status_code == 404

        result = app_client().get(ENDPOINT + str(gliders.ids[1]))
        assert result.status_code == 200

    def test_glider_cannot_be_deleted_with_associated_flight(self, app_client, flights):

        ### Action ###

        glider_id = flights.data[0].glider_id
        resp = app_client().delete(ENDPOINT + str(glider_id))

        ### Tests ###

        assert resp.status_code == 400

        result = app_client().get(ENDPOINT + str(glider_id))
        assert result.status_code == 200
