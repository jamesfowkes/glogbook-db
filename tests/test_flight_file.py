import pytest
import os
import json

from collections import OrderedDict

from tests import JSONResponseHelper, UnitTestDataHelper, create_json_data
from tests.fixtures import app, app_client, flights, filetypes

from app.models.flight_file import FlightFile

FLIGHT_FILES = [
    {"link": "http://example.com/video.mp4"},
    {"link": "http://example.com/trace.txt"},
]

ENDPOINT = '/api/flight_file/'

def create_test_flight_files(data_list, flights, filetypes):

    with app.app_context():
        flight_files = {}
        for data, flight_id, filetype_id in zip(data_list, flights.ids, filetypes.ids):
            data = data.copy()
            data["flight_id"] = flight_id
            data["filetype_id"] = filetype_id
            flight_file = FlightFile(**data)
            flight_file.save()
            flight_files[flight_file["flight_file_id"]] = flight_file

    return UnitTestDataHelper(flight_files)

def create_flight_files_by_request(client, data_list, flights, filetypes):

    responses = []
    file_ids = []
    for data, flight_id, filetype_id in zip(data_list, flights.ids, filetypes.ids):
        data_copy = data.copy()
        data_copy["flight_id"] = flight_id
        data_copy["filetype_id"] = filetype_id
        data_json = json.dumps(data_copy)

        response = client().post(ENDPOINT, data=data_json, headers={'content-type': 'application/json'}) 
        response = JSONResponseHelper(response)
        responses.append(response)
        file_ids.append(response.json["flight_file_id"])

    return file_ids, responses

def create_flight_file_data(data, flight_id, filetype_id):
    data_copy = data.copy()
    data_copy["flight_id"] = flight_id
    data_copy["filetype_id"] = filetype_id
    return data_copy


class TestFlightFile:

    def test_flight_file_creation_valid_data(self, app_client, flights, filetypes):

        ### Action ###

        _, responses = create_flight_files_by_request(app_client, FLIGHT_FILES, flights, filetypes)

        ### Tests ###
            
        for resp, expected_data, flight_id, filetype_id in zip(responses, FLIGHT_FILES, flights.ids, filetypes.ids):
            assert resp.status_code == 201
            assert resp.json["flight_id"] == flight_id
            assert resp.json["filetype_id"] == filetype_id
            assert resp.json["link"] == expected_data["link"]

    def test_flight_file_creation_missing_data_all_fields(self, app_client):

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=json.dumps({}), headers={'content-type': 'application/json'}))
        
        ### Tests ###

        assert resp.status_code == 400
        for key in FLIGHT_FILES[0].keys():
            assert key in resp.json["errors"]

    @pytest.mark.parametrize("missing_field", ["flight_id", "filetype_id", "link"])
    def test_flight_file_creation_missing_fields(self, app_client, missing_field):

        ### Setup ###

        invalid_data = create_flight_file_data(FLIGHT_FILES[0], 1, 1)
        invalid_data.pop(missing_field)

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=json.dumps(invalid_data), headers={'content-type': 'application/json'}))

        ### Tests ###

        assert resp.status_code == 400
        assert missing_field in resp.json["errors"]

    @pytest.mark.parametrize("present_field,value",
        [
            ('flight_id', 1),
            ('filetype_id', 1),
            ('link', "http://example.com/somefile"),
        ]
    )
    def test_flight_file_creation_missing_data_all_but_one_field(self, app_client, present_field, value):

        ### Action ###

        res = app_client().post(ENDPOINT, data=json.dumps({present_field:value}), headers={'content-type': 'application/json'})
        json_data = json.loads(res.data)

        ### Tests ###

        expected_missing_fields = ['flight_id', 'filetype_id', 'link']
        expected_missing_fields.remove(present_field)

        assert res.status_code == 400
        for expected_missing_field in expected_missing_fields:
            assert expected_missing_field in json_data["errors"]

    @pytest.mark.parametrize("field, value",
        [
            ('flight_id', "1"),
            ('filetype_id', "1"),
            ('link', 0),
        ]
    )
    def test_flight_file_creation_invalid_field_datatype(self, app_client, field, value):

        ### Setup ###

        invalid_data = create_flight_file_data(FLIGHT_FILES[0], 1, 1)
        invalid_data[field] = value

        ### Action ###

        res = app_client().post(ENDPOINT, data=json.dumps(invalid_data), headers={'content-type': 'application/json'})
        json_data = json.loads(res.data)

        ### Tests ###

        assert res.status_code == 400
        assert field in json_data["errors"]
        assert str(value) in json_data["errors"][field]

    def test_api_can_get_all_flight_files(self, app_client, flights, filetypes):
        
        ### Setup ###

        flight_files = create_test_flight_files(FLIGHT_FILES, flights, filetypes)

        ### Action ###

        resp = JSONResponseHelper(app_client().get(ENDPOINT))
        print(resp.json)
        ### Tests ###

        assert resp.status_code == 200

        for json_resp, file_id, flight_id, filetype_id, expected_data in zip(resp.json, flight_files.ids, flights.ids, filetypes.ids, FLIGHT_FILES):
            assert json_resp["flight_id"] == flight_id
            assert json_resp["flight_file_id"] == file_id
            assert json_resp["filetype_id"] == filetype_id
            assert json_resp["link"] == expected_data["link"]

    def test_api_can_get_flight_file_by_id(self, app_client, flights, filetypes):

        ### Setup ###

        flight_files = create_test_flight_files(FLIGHT_FILES, flights, filetypes)

        ### Action ###

        responses = []
        for flight_file_id in flight_files.ids:
            resp = JSONResponseHelper(app_client().get(ENDPOINT + str(flight_file_id)))
            responses.append(resp)

        ### Tests ###

        zipped_data = zip(responses, flight_files.ids, flights.ids, filetypes.ids, FLIGHT_FILES)
        for response, expected_file_id, flight_id, filetype_id, expected_flight_file in zipped_data: 
            assert response.status_code == 200
            assert response.json["flight_id"] == flight_id
            assert response.json["flight_file_id"] == expected_file_id
            assert response.json["filetype_id"] == filetype_id
            assert response.json["link"] == expected_flight_file["link"]

    def test_flight_file_can_be_edited_with_valid_data(self, app_client, flights, filetypes):

        ### Setup ###

        flight_files = create_test_flight_files(FLIGHT_FILES, flights, filetypes)
        flight_file_id = flight_files.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(flight_file_id), data=json.dumps({"link": "http://example.co.uk/otherfile.kml"}),
            headers={'content-type': 'application/json'}
        )

        ### Tests ###

        assert resp.status_code == 200

        resp = JSONResponseHelper(app_client().get(ENDPOINT + str(flight_file_id)))
            
        assert resp.json["link"] == "http://example.co.uk/otherfile.kml"

    @pytest.mark.parametrize("field, value",
        [
            ('filetype_id', "1"),
            ('link', 0),
        ]
    )
    def test_flight_file_cannot_be_edited_with_invalid_data(self, app_client, flights, filetypes, field, value):

        ### Setup ###

        flight_files = create_test_flight_files(FLIGHT_FILES, flights, filetypes)
        flight_file_id = flight_files.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(flight_file_id), data=json.dumps({field:value}),
            headers={'content-type': 'application/json'}
        )

        ### Tests ###

        assert resp.status_code == 400

        assert field in resp.json["errors"]
        assert str(value) in resp.json["errors"][field]

        resp = JSONResponseHelper(app_client().get(ENDPOINT + str(flight_file_id)))

        assert resp.json == flight_files.data[0].data()

    def test_flight_file_deletion(self, app_client, flights, filetypes):

        ### Setup ###

        flight_files = create_test_flight_files(FLIGHT_FILES, flights, filetypes)

        ### Action ###

        resp = app_client().delete(ENDPOINT + str(flight_files.ids[0]))

        ### Tests ###

        assert resp.status_code == 200

        result = app_client().get(ENDPOINT + str(flight_files.ids[0]))
        assert result.status_code == 404

        result = app_client().get(ENDPOINT + str(flight_files.ids[1]))
        assert result.status_code == 200
