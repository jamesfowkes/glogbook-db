import pytest
import os
import json

from collections import OrderedDict

from flask import session

from tests import JSONResponseHelper, UnitTestDataHelper, create_json_data
from tests.fixtures import app, app_client, flights

from app.models.location import Location

LOCATIONS = [
    {
        "name": "Rattlesden",
        "description": "Rattlesden Gliding Club",
        "lat": 52.169661,
        "lon": 0.875431
    },
    {
        "name": "Portmoak",
        "description": "Scottish Gliding Centre",
        "lat": 56.189034,
        "lon": -3.322024
    }
]

ENDPOINT = '/api/location/'

def create_test_locations(data_list):

    with app.app_context():
        locations = {}
        for data in data_list:
            location = Location(**data)
            location.save()
            locations[location["location_id"]] = location

    return UnitTestDataHelper(locations)

def create_locations_by_request(client, data_list):

    responses = []
    location_ids = []
    for data in data_list:
        data_json = json.dumps(data)
        response = client().post(ENDPOINT, data=data_json, headers={'content-type': 'application/json'}) 
        response = JSONResponseHelper(response)
        responses.append(response)
        location_ids.append(response.json["location_id"])

    return location_ids, responses

class TestLocation:

    def test_location_creation_valid_data(self, app_client):

        ### Action ###

        _, responses = create_locations_by_request(app_client, LOCATIONS)

        ### Tests ###
        
        for resp, expected_data in zip(responses, LOCATIONS):
            assert resp.status_code == 201
            resp.json.pop("location_id")
            assert resp.json == expected_data

    def test_location_creation_missing_data_all_fields(self, app_client):

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=json.dumps({}), headers={'content-type': 'application/json'}))
        
        ### Tests ###

        assert resp.status_code == 400        
        
        for key in LOCATIONS[0].keys():
            assert key in resp.json["errors"]

    @pytest.mark.parametrize("missing_field", ['name', 'description', 'lat', 'lon'])
    def test_location_creation_missing_data_one_field(self, app_client, missing_field):

        ### Setup ###

        invalid_data = create_json_data(LOCATIONS[0], missing_fields=[missing_field])

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'}))

        ### Tests ###

        assert resp.status_code == 400
        assert missing_field in resp.json["errors"]

    def test_location_creation_missing_data_several_fields(self, app_client):

        ### Setup ###

        invalid_data = create_json_data(LOCATIONS[0], missing_fields=["name", "lat"])

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'}))
        
        ### Tests ###

        assert resp.status_code == 400
        assert "name" in resp.json["errors"]
        assert "lat" in resp.json["errors"]

    @pytest.mark.parametrize("field, value",
        [
            ("name", 0),
            ("description", 0),
            ("lat", "52.12345"),
            ("lon", "-0.7654321")
        ]
    )
    def test_location_creation_invalid_data(self, app_client, field, value):

        ### Setup ###

        invalid_data = create_json_data(LOCATIONS[0], invalid_fields={field:value})

        ### Action ###

        resp = JSONResponseHelper(app_client().post(ENDPOINT, data=invalid_data, headers={'content-type': 'application/json'}))

        assert resp.status_code == 400
        assert field in resp.json["errors"]
        assert str(value) in resp.json["errors"][field]

    def test_api_can_get_all_locations(self, app_client):
        
        ### Setup ###

        locations = create_test_locations(LOCATIONS)

        ### Action ###

        resp = JSONResponseHelper(app_client().get(ENDPOINT))

        ### Tests ###

        assert resp.status_code == 200

        assert resp.json[0].pop("location_id") == locations.ids[0]
        assert resp.json[1].pop("location_id") == locations.ids[1]
        
        assert resp.json[0] == LOCATIONS[0]
        assert resp.json[1] == LOCATIONS[1]


    def test_api_can_get_location_by_id(self, app_client):

        ### Setup ###

        locations = create_test_locations(LOCATIONS)
        
        ### Action ###

        responses = []
        for location_id in locations.ids:
            resp = JSONResponseHelper(app_client().get(ENDPOINT + str(location_id)))
            responses.append(resp)

        ### Tests ###

        for response, expected_location_id, expected_location in zip(responses, locations.ids, LOCATIONS): 
            assert response.status_code == 200
            assert response.json["location_id"] == expected_location_id
            assert response.json["name"] == expected_location["name"]
            assert response.json["description"] == expected_location["description"]
            assert response.json["lat"] == expected_location["lat"]
            assert response.json["lon"] == expected_location["lon"]

    def test_location_can_be_edited_with_valid_data(self, app_client):

        ### Setup ###

        locations = create_test_locations(LOCATIONS)
        location_id = locations.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(location_id), data=json.dumps({"name": "Long Mynd", "description": "Midland Gliding Club", "lat": 52.518064, "lon": -2.880158}),
            headers={'content-type': 'application/json'}
        )

        ### Tests ###

        assert resp.status_code == 200

        resp = JSONResponseHelper(app_client().get(ENDPOINT + str(location_id)))
            
        assert resp.json["name"] == "Long Mynd"
        assert resp.json["description"] == "Midland Gliding Club"
        assert resp.json["lat"] == 52.518064
        assert resp.json["lon"] == -2.880158

    @pytest.mark.parametrize("field, value",
        [
            ("name", 0),
            ("description", 0),
            ("lat", "52.12345"),
            ("lon", "-0.7654321")
        ]
    )
    def test_location_cannot_be_edited_with_invalid_data(self, app_client, field, value):

        ### Setup ###

        locations = create_test_locations(LOCATIONS)
        location_id = locations.ids[0]

        ### Action ###

        resp = app_client().put(
            ENDPOINT + str(location_id), data=json.dumps({field:value}),
            headers={'content-type': 'application/json'}
        )

        ### Tests ###


        assert resp.status_code == 400
        assert field in resp.json["errors"]
        assert str(value) in resp.json["errors"][field]

        resp = JSONResponseHelper(app_client().get(ENDPOINT + str(location_id)))
        assert resp.json["name"] == LOCATIONS[0]["name"]
        assert resp.json["description"] == LOCATIONS[0]["description"]
        assert resp.json["lat"] == LOCATIONS[0]["lat"]
        assert resp.json["lon"] == LOCATIONS[0]["lon"]

    def test_location_deletion(self, app_client):

        ### Setup ###

        locations = create_test_locations(LOCATIONS)

        ### Action ###

        resp = app_client().delete(ENDPOINT + str(locations.ids[0]))

        ### Tests ###

        assert resp.status_code == 200

        result = app_client().get(ENDPOINT + str(locations.ids[0]))
        assert result.status_code == 404

        result = app_client().get(ENDPOINT + str(locations.ids[1]))
        assert result.status_code == 200


    def test_location_cannot_be_deleted_with_associated_flight(self, app_client, flights):

        ### Action ###

        location_id = flights.data[0].location_id
        resp = app_client().delete(ENDPOINT + str(location_id))

        ### Tests ###

        assert resp.status_code == 400

        result = app_client().get(ENDPOINT + str(location_id))
        assert result.status_code == 200
