from collections import OrderedDict

class BaseConfig():
    DEBUG = False
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class CIConfig(BaseConfig):
    SECRET_KEY = b'ci'
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg://postgres:postgres@postgres:5432/glogbook_ci'
    LOGFILE="tests/applog"
    DEBUG = True

class TestConfig(BaseConfig):
    SECRET_KEY = b'test'
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg://glogbook_user:18AuNzfJ@localhost/glogbook_test'
    LOGFILE="tests/applog"
    DEBUG = True

class RunConfig(BaseConfig):
    SECRET_KEY = b'run' # Replace on production server with actual secret key
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg://glogbook_user:18AuNzfJ@localhost/glogbook_db'
    LOGFILE="tests/applog"
    DEBUG = True
