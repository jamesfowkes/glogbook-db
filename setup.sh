#!/bin/bash

# Setup logging
sudo mkdir -p /var/log/glogbook
sudo chown $USER:$USER /var/log/glogbook

# Setup virtual environment

python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
