# glogbook-db

A logbook database and API for gliding

## Installation

### Pre-Requisites

#### Debian

Install the following packages: postgresql libpq-dev libxslt1-dev python-dev

### Steps

Create a glogbook_db user in postgres:

`su - postgres`

`createuser --interactive --pwprompt` and follow the prompts, grant all permissions to the glogbook_db user.

and a glogbook database:

`createdb glogbook`

Run setup.sh. You will be asked for the root password.

## Initial Database Creation

The environmental variable `CONFIG_CLASS` must exist and should be set to `config.RunConfig`