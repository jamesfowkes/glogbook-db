alembic==1.14.0
aniso8601==8.0.0
asn1crypto==1.4.0
atomicwrites==1.4.0
attrs==19.3.0
blinker==1.9.0
cffi==1.14.1
click==8.1.7
colorama==0.4.6
comtypes==1.4.8
cryptography==43.0.3
dateparser==1.2.0
docopt==0.6.2
Flask==3.0.3
Flask-JSON==0.4.0
Flask-Migrate==4.0.7
flask-restx==1.3.0
Flask-SQLAlchemy==3.1.1
greenlet==3.1.1
idna==2.10
importlib-metadata==1.7.0
importlib_resources==6.4.5
iniconfig==1.0.0
ipaddr==2.2.0
itsdangerous==2.2.0
Jinja2==3.1.4
jsonschema==3.2.0
lxml==5.3.0
Mako==1.1.3
MarkupSafe==3.0.2
more-itertools==8.4.0
ordereddict==1.1
packaging==20.4
pluggy==1.5.0
protobuf==3.9.1
psycopg==3.2.3
psycopg-binary==3.2.3
py==1.11.0
pycparser==2.20
pyOpenSSL==19.0.0
pyparsing==2.4.7
pypiwin32==223
pyrsistent==0.16.0
pytest==8.3.4
python-dateutil==2.8.1
python-editor==1.0.4
pyttsx3==2.98
pytz==2020.1
pywin32==308
regex==2024.11.6
setuptools==75.3.0
six==1.16.0
SQLAlchemy==2.0.36
tabulate==0.9.0
toml==0.10.1
typing_extensions==4.12.2
tzdata==2024.2
tzlocal==5.2
wcwidth==0.2.5
Werkzeug==3.1.3
zipp==3.1.0
